/*
 */


#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>  /* uprintf */
#include <sys/linker.h>
#include <sys/errno.h>
#include <sys/unistd.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/proc.h>
#include <sys/syscall.h>


#define P_WORD "french revolution"

/* ref: https://www.freebsd.org/cgi/man.cgi?write(2) */
/* write system call hook */
static int write_hook(struct thread *td, void* syscall_args){
  struct write_args {
    int fd;
    const void *buf;
    size_t nbytes;
  } *uap;

  uap = (struct write_args *)syscall_args;

  /* probably "escalation" code here */
  if(strstr(uap->buf, P_WORD) != NULL) {
  	uprintf("Password found in text\n");
  }

  return(sys_write(td, syscall_args));
}

/* The function called at load/unload. */
static int load(struct module *module, int cmd, void *arg){
  int error = 0;

  switch(cmd)
  {
    case MOD_LOAD:
      /* Replace write with mkdir_hook. */
      sysent[SYS_write].sy_call = (sy_call_t *)write_hook;
      break;

    case MOD_UNLOAD:
      /* Change everthing back to normal. */
      sysent[SYS_write].sy_call = (sy_call_t *)sys_write;
      break;

    default:
      error = EOPNOTSUPP;
      break;
  }
  return(error);
}

static moduledata_t write_hook_mod = {
  "write_hook",       /* module name */
  load,               /* event handler */
  NULL                /* extra data */
};

DECLARE_MODULE(write_hook, write_hook_mod, SI_SUB_DRIVERS, SI_ORDER_MIDDLE);
