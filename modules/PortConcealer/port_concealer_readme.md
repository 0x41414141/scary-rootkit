####Port Concealer
#####Description
The Port Concealer (_PortConcealer.c_) contains the textbook based basic design for hiding ports on the system. As it is based on the textbook's design, at present it only hides TCP ports.
#####Flow
* Iterate the tcbinfo structure which contains all TCP Internet Protocol Control Block (inpcb)
* Lock the entry
* If this is the port we want to hide:
 * Remove the port from the tdbinfo structure
* Unlock the entry
* Unlock the tcbinfo structure
* Register the module
 
#####Module Function
* Create the function to load/umload
* If:
 * The function is to Load, print the offset
 * The function is to Unload, print that the system is unloaded
