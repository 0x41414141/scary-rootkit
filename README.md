# Rootkit Assignment
**Team:** `41414141`

## Rootkit Installation
The rootkit is comprised of multiple LKM’s (Loadable Kernel Modules) which are loaded on installation.

All modules are precompiled binaries so they are ready to be loaded into the kernel.

The installation is done in the following steps:
1. The `rootkit.tar` file is loaded onto the target server and extracted.
2. Each module is installed using `sudo kldload ./<module_name>.ko`
    1. `sudo kldload ./priv_esc.ko`
    2. `sudo kldload ./PortConcealer.ko`
    3. `sudo kldload ./ProcessConcealer.ko`
    4. `sudo kldload ./ModuleHider.ko`
3. The rootkit makes itself persistent by:
By adding an entry to the crontab under `/etc/crontab`
xoring the rootkit.tar file and saving it to `/var/log/maillog.100.dbz` to make it seems like a simple log file
Adding a `/var/log/revive.log` binary which check if the rootkit is still alive otherwise it will  xor the tar file again, moves it to /tmp and installs it
4. The rootkit folder is then removed using `rm -rf <folder_path>`
5. The `rootkit.tar` file is removed

The LKMs are now loaded into the kernel and are hidden, allowing a non-root user to get a root shell without the use of a password.


### Privilege Escalation
The privilege escalation module creates a backdoor on the filesystem by creating a new `syscall` which is passed its own process id that it then sets it’s `uid` and `gid` to `0` respectively to elevate to root.

To get a root shell, a non-root user can call the created system call and then spawn a shell with the newly acquired privileges. He will then be able to perform any actions as root until the process ends or is killed off.


### Rootkit Concealment
The Rootkit will be concealed primarily by the `ModuleHider.ko` module which will hide all the other loaded modules:
* `RootEsc.ko`
* `PortConcealer.ko`
* `ProcessConcealer.ko`
including itself, by removing their entries in the `linker_files` list, and the `modules` list in the kernel's lists.

Furthermore, after the installation, we will essentially "delete" the binary files from the file system as they should be loaded in memory, leaving no footprint for `find` etc. as there will be no physical file left. Naturally, when we attempt the persistence, we will have to reverse this design.


### Process Concealer
A rudimentary Process Concealer (`ProcessConcealer.ko`) has been developed based on the basic example from the course textbook (redux version) which takes in a process id as an argument and hides it by removing it from the `allprocs` table and `pidhashbl` table. The `nprocs` variable is decremented to adjust the process count as well as removing it from the process group. 


### Port Concealer 
A rudimentary Port Concealer (`PortConcealer.ko`) has been developed based on the basic example from the course textbook which takes in a port number of a TCP connection as an argument and hides the connection by removing it from the `tcbinfo` list. 


## Detection
The following items are used to implement detection of rootkits:
* `uland_sysent_detector`
* `kland_sysent_detector`
* `LogDetector`

They run in the order listed above and if any of the modules returns 1 (meaning we detected a rootkit) the detect script stops and simply returns 1 (rootkit found).

### uland_sysent_detector
The Sysent Detector, `uland_sysent_detector` is a userland program used to detect whether system calls have been hooked or not by examining all the system calls and ensuring that they are pointed to the correct addresses. If they are not pointing to the correct addresses, then they have been hooked.

We know the correct address of a system call from their object files which contains their correct address offsets which in turn gives the correct address. A hooked system call would be pointing to another address as it needs to process it's hook.

We also check that the syscall entries that are supposed to be empty are actually empty. And if they aren’t we know that someone has create a new syscall entry and that there is a kernel rootkit.

### kland_sysent_detector
`kland_sysent_detector` is another way for the detector to make sure no module is attached to the kernel sysent table. It is a kernel module and, similarly to `uland_sysent_detector`, it checks that the system table entries are as they should be. It also tests the kldload functionality and makes sure that it has not been disabled by a rootkit. 

The idea behind kland_sysent_detector is to make sure that even if `kvm_read` has been patched, we are still able to check for syscall entries since we load our module in the kernel and execute from there.

These first 2 modules should allow us to catch kernel sys_table modifications in most cases.

### Log Detector
The log detector is a userland program that reads through the following entries:
* `/var/loguid.today"`
* `/var/loguid.yesterday"`
* `/root/.history"`
* `/var/log/auth.log"`
* `/etc/crontab"`
It searches these to find keyword matches of:
* `kldload`

It also searches these files to find anomalies in the file contents eg. in `setuid` log files, the paths of the command is listed and any entries where the paths of the commands are not normal are flagged.

The Log Detector allows us to catch rootkits if they have been noisy when installing/escalating and haven’t cleaned up after themselves.


### Bonus: Persistence
Reboot persistence is achieved by storing an xor encrypted `rootkit.tar` file and periodically checking if the rootkit is still alive by executing the hidden syscalls. If it is unable to find the syscall, it will attempt to reinstall the rootkit by copying it to the `/tmp` folder and using the usual installation script. The installation script will cleanup after itself and leave no trace in the /tmp folder.
This is achieved by the following:
* logs and encrypted rootkit tar file stored at the `/var/log/` directory, posing as a log file entries.
* A revive log added to crontab as a job
* After reboot,  `/var/log/revive.log` will be executed, examining the integrity of rootkit, in which if it is not present, reinstalls the rootkit at `/tmp/cutebunny/`
When running, the revive log checks for the presence of the rootkit in memory (via syscall to the escalation module) and if it is not present, then executes the revival. Otherwise it continues on as normal. This is to prevent cron constantly respawning the rootkit which would probably lead to system stability problems and ultimately the detection of the rootkit.


### Remove logs
To hide our module more thoroughly, we added a shell script to clean up after installation. Following detection, we check log entries including:
* `/var/loguid.today"`
* `/var/loguid.yesterday"`
* `/root/.history"`
* `/var/log/auth.log"`
* `/etc/crontab"`
To find any matching possible call for shells or other dubious entries. If any are found, the log file is removed. Although we remove the entire log file we gather that these files are hardly looked at and at times usually empty to begin with and as such is likely to be overlooked.

## Bonus Marks to Be Attempted
We were aiming to attempt all the bonus marks based on the following priorities (according to time and difficulty). However we were only able to achieve the following.
* Persistence
As mentioned previously, this was done via cron periodically checking every minute to see if the rootkit is in memory via a syscall to the escalation module and if it isn't, reinstalls itself via it's files which are hidden as `/tmp/cutebunny.tar`.


### Changes since midpoint submission
Since the midpoint submission, we have update a number of features on our rootkit:
* We’ve modified the installation script to make it cleanup more efficiently after itself and remove noise from the machine.
* We’ve modified our hidden privilege escalation in order to be able to query it and check if the rootkit is still alive or not
* We’ve added persistence to our rootkit in order to stay alive after reboots and/or after being unloaded. This makes the rootkit harder to detect and therefore much more intrusive
* We have added a detection module which attempts to detect rootkits as explained in previous sections.


### Rootkit Design decisions 
We wanted our rootkit to be relatively well hidden while also being hard to remove.
The first design decision was to choose between escalating to root from userland or kerneland. We decided to make the escalation work from kerneland as it was harder to detect than having a suid executable in userland.

By removing the rootkit after installation we made sure to remove the source from disk where it could be seen and recognised.

To hide the rootkit, we chose to remove the modules we were using from the linker_files table. This allowed us to be hidden from commands such as `kldstat` etc. 

For persistence, we chose to update the crontab entries as a normal user could very well be updating crontab and adding their own entries to it. Our rootkit was hidden as a log file and xoring it made sure any simple attempts at grepping for sensitive keywords would not return anything.


### Detector Design decisions
Our main detection effort is in detecting syscall entries as most rootkit will need to modify the sysent table in order to hide. 

We first attempt to read kernel memory from userland and check for potentially invalid entries. We don’t have access to the `nosys` and `lkmnosys` pointers so instead we assume that the empty entries should point to 1 of 2 possible addresses. If we find more than 2 addresses, we assume that the kernel sys_table has been modified.

After reading the midsem design files, we saw that some rootkits might be modifying kvm read. Therefore to make our system table modifications more accurate, we added another module which acted similarly to our userland module except he was checking it from kernel land and therefore bypassing any kvm read modifications. During loading time, the module checks for anomalies such as not being able to load into kernel and checks the errno returned value to see if it could mean that a rootkit is present.

Finally we decided to add a log grepper module which looks at different logs and checks the validity of those logs. For example, by looking at the setuid logs, it checks that the binaries which modified the uid of a process had permission to do so. It also checks for odd entries in files such as crontab entries and boot.conf. This module was made with the idea that when first running and installing the module and escalating, this will be very noisy and could easily end up with odd entries in log files.
