import sys
import os
import glob

pwd = os.getcwd()

def glob_exec(glob_str, cmd_template):
    global pwd

    print("[+] Running glob_exec : glob = '{}' || cmd_template = '{}'".format(glob_str, cmd_template))
    for filename in glob.iglob(glob_str, recursive=True):
        split_filename = filename.split("/")
        module_dir = "{}/{}".format(
                pwd,
                "/".join(split_filename[:-1])
        )
        fn = "./{}".format(split_filename[-1])

        os.chdir(module_dir)

        #print("cwd = {} || module_dir = {} || fn = {}".format(os.getcwd(), module_dir, fn))

        print("    [-] Path: {} -> Executing cmd: `{}`".format(filename, cmd_template.format(fn)))
        os.system(cmd_template.format(fn))
        os.chdir(pwd)


actions = dict(
    compile= lambda : glob_exec("./modules/**/compile.sh", "sh {}"),
    load= lambda : glob_exec("./modules/**/load.sh", "sh {}"),
    unload= lambda : glob_exec("./modules/**/unload.sh", "sh {}"),
    echo= lambda : glob_exec("./modules/**/compile.sh", "echo {}")
)


if len(sys.argv) < 2 or sys.argv[1] not in actions:
    print("Usage: python3 core.py <{}>".format("|".join(actions.keys())))
    exit()

actions[sys.argv[1]]()

