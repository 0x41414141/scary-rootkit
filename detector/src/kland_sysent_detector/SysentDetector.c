#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/linker.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/proc.h>
#include <sys/syscall.h>
#include <sys/file.h>
#include <sys/malloc.h>
#include <sys/types.h>
#include <sys/lock.h>


int is_empty_syscall(int i);

const int empty_syscalls[] = {8,11,19,38,40,46,48,52,62,63,64,67,68,71,76,77,84,87,88,91,94,99,101,102,103,107,108,109,110,111,112,113,114,115,119,125,129,130,139,141,142,143,144,145,146,149,150,151,152,153,159,167,168,172,177,178,179,180,184,185,186,187,193,208,210,211,212,213,214,215,216,217,218,219,223,245,246,249,258,259,260,261,262,263,264,265,266,267,268,269,270,271,273,281,282,283,284,285,286,287,288,291,292,293,294,295,296,313,322,323,364,365,366,367,368,369,370,375,379,380,381,382,383,399,418,419,420,434,435,440,467,468,469,470,514,521};

#define n_empty_syscalls (sizeof(empty_syscalls) / (sizeof(const int *)))

struct sys_detect_args {
    int to_ignore;
};

int is_empty_syscall(int i) {
    for(int idx = 0; idx < n_empty_syscalls; idx++)
    if (empty_syscalls[idx] == i)
    return 1;
    return 0;
}


static int
sys_detect_handler (struct thread* td, void* syscall_arguments)
{
    struct sys_detect_args *sda;
    sda = (struct sys_detect_args *) syscall_arguments;

    int error_count = 0;

    if (sysent[SYS_fork].sy_call!=(sy_call_t *)(sys_fork)) error_count++;
    if (sysent[SYS_read].sy_call!=(sy_call_t *)(sys_read)) error_count++;
    if (sysent[SYS_write].sy_call!=(sy_call_t *)(sys_write)) error_count++;
    if (sysent[SYS_open].sy_call!=(sy_call_t *)(sys_open)) error_count++;
    if (sysent[SYS_close].sy_call!=(sy_call_t *)(sys_close)) error_count++;
    if (sysent[SYS_wait4].sy_call!=(sy_call_t *)(sys_wait4)) error_count++;
    if (sysent[SYS_link].sy_call!=(sy_call_t *)(sys_link)) error_count++;
    if (sysent[SYS_unlink].sy_call!=(sy_call_t *)(sys_unlink)) error_count++;
    if (sysent[SYS_chdir].sy_call!=(sy_call_t *)(sys_chdir)) error_count++;
    if (sysent[SYS_fchdir].sy_call!=(sy_call_t *)(sys_fchdir)) error_count++;
    if (sysent[SYS_mknod].sy_call!=(sy_call_t *)(sys_mknod)) error_count++;
    if (sysent[SYS_chmod].sy_call!=(sy_call_t *)(sys_chmod)) error_count++;
    if (sysent[SYS_chown].sy_call!=(sy_call_t *)(sys_chown)) error_count++;
    if (sysent[SYS_getpid].sy_call!=(sy_call_t *)(sys_getpid)) error_count++;
    if (sysent[SYS_mount].sy_call!=(sy_call_t *)(sys_mount)) error_count++;
    if (sysent[SYS_unmount].sy_call!=(sy_call_t *)(sys_unmount)) error_count++;
    if (sysent[SYS_setuid].sy_call!=(sy_call_t *)(sys_setuid)) error_count++;
    if (sysent[SYS_getuid].sy_call!=(sy_call_t *)(sys_getuid)) error_count++;
    if (sysent[SYS_geteuid].sy_call!=(sy_call_t *)(sys_geteuid)) error_count++;
    if (sysent[SYS_ptrace].sy_call!=(sy_call_t *)(sys_ptrace)) error_count++;
    if (sysent[SYS_recvmsg].sy_call!=(sy_call_t *)(sys_recvmsg)) error_count++;
    if (sysent[SYS_sendmsg].sy_call!=(sy_call_t *)(sys_sendmsg)) error_count++;
    if (sysent[SYS_recvfrom].sy_call!=(sy_call_t *)(sys_recvfrom)) error_count++;
    if (sysent[SYS_accept].sy_call!=(sy_call_t *)(sys_accept)) error_count++;
    if (sysent[SYS_getpeername].sy_call!=(sy_call_t *)(sys_getpeername)) error_count++;
    if (sysent[SYS_getsockname].sy_call!=(sy_call_t *)(sys_getsockname)) error_count++;
    if (sysent[SYS_access].sy_call!=(sy_call_t *)(sys_access)) error_count++;
    if (sysent[SYS_chflags].sy_call!=(sy_call_t *)(sys_chflags)) error_count++;
    if (sysent[SYS_fchflags].sy_call!=(sy_call_t *)(sys_fchflags)) error_count++;
    if (sysent[SYS_sync].sy_call!=(sy_call_t *)(sys_sync)) error_count++;
    if (sysent[SYS_kill].sy_call!=(sy_call_t *)(sys_kill)) error_count++;
    if (sysent[SYS_getppid].sy_call!=(sy_call_t *)(sys_getppid)) error_count++;
    if (sysent[SYS_dup].sy_call!=(sy_call_t *)(sys_dup)) error_count++;
    if (sysent[SYS_getegid].sy_call!=(sy_call_t *)(sys_getegid)) error_count++;
    if (sysent[SYS_profil].sy_call!=(sy_call_t *)(sys_profil)) error_count++;
    if (sysent[SYS_ktrace].sy_call!=(sy_call_t *)(sys_ktrace)) error_count++;
    if (sysent[SYS_getgid].sy_call!=(sy_call_t *)(sys_getgid)) error_count++;
    if (sysent[SYS_getlogin].sy_call!=(sy_call_t *)(sys_getlogin)) error_count++;
    if (sysent[SYS_setlogin].sy_call!=(sy_call_t *)(sys_setlogin)) error_count++;
    if (sysent[SYS_acct].sy_call!=(sy_call_t *)(sys_acct)) error_count++;
    if (sysent[SYS_sigaltstack].sy_call!=(sy_call_t *)(sys_sigaltstack)) error_count++;
    if (sysent[SYS_ioctl].sy_call!=(sy_call_t *)(sys_ioctl)) error_count++;
    if (sysent[SYS_reboot].sy_call!=(sy_call_t *)(sys_reboot)) error_count++;
    if (sysent[SYS_revoke].sy_call!=(sy_call_t *)(sys_revoke)) error_count++;
    if (sysent[SYS_symlink].sy_call!=(sy_call_t *)(sys_symlink)) error_count++;
    if (sysent[SYS_readlink].sy_call!=(sy_call_t *)(sys_readlink)) error_count++;
    if (sysent[SYS_execve].sy_call!=(sy_call_t *)(sys_execve)) error_count++;
    if (sysent[SYS_umask].sy_call!=(sy_call_t *)(sys_umask)) error_count++;
    if (sysent[SYS_chroot].sy_call!=(sy_call_t *)(sys_chroot)) error_count++;
    if (sysent[SYS_msync].sy_call!=(sy_call_t *)(sys_msync)) error_count++;
    if (sysent[SYS_vfork].sy_call!=(sy_call_t *)(sys_vfork)) error_count++;
    if (sysent[SYS_sbrk].sy_call!=(sy_call_t *)(sys_sbrk)) error_count++;
    if (sysent[SYS_sstk].sy_call!=(sy_call_t *)(sys_sstk)) error_count++;
    if (sysent[SYS_munmap].sy_call!=(sy_call_t *)(sys_munmap)) error_count++;
    if (sysent[SYS_mprotect].sy_call!=(sy_call_t *)(sys_mprotect)) error_count++;
    if (sysent[SYS_madvise].sy_call!=(sy_call_t *)(sys_madvise)) error_count++;
    if (sysent[SYS_mincore].sy_call!=(sy_call_t *)(sys_mincore)) error_count++;
    if (sysent[SYS_getgroups].sy_call!=(sy_call_t *)(sys_getgroups)) error_count++;
    if (sysent[SYS_setgroups].sy_call!=(sy_call_t *)(sys_setgroups)) error_count++;
    if (sysent[SYS_getpgrp].sy_call!=(sy_call_t *)(sys_getpgrp)) error_count++;
    if (sysent[SYS_setpgid].sy_call!=(sy_call_t *)(sys_setpgid)) error_count++;
    if (sysent[SYS_setitimer].sy_call!=(sy_call_t *)(sys_setitimer)) error_count++;
    if (sysent[SYS_swapon].sy_call!=(sy_call_t *)(sys_swapon)) error_count++;
    if (sysent[SYS_getitimer].sy_call!=(sy_call_t *)(sys_getitimer)) error_count++;
    if (sysent[SYS_getdtablesize].sy_call!=(sy_call_t *)(sys_getdtablesize)) error_count++;
    if (sysent[SYS_dup2].sy_call!=(sy_call_t *)(sys_dup2)) error_count++;
    if (sysent[SYS_fcntl].sy_call!=(sy_call_t *)(sys_fcntl)) error_count++;
    if (sysent[SYS_select].sy_call!=(sy_call_t *)(sys_select)) error_count++;
    if (sysent[SYS_fsync].sy_call!=(sy_call_t *)(sys_fsync)) error_count++;
    if (sysent[SYS_setpriority].sy_call!=(sy_call_t *)(sys_setpriority)) error_count++;
    if (sysent[SYS_socket].sy_call!=(sy_call_t *)(sys_socket)) error_count++;
    if (sysent[SYS_connect].sy_call!=(sy_call_t *)(sys_connect)) error_count++;
    if (sysent[SYS_getpriority].sy_call!=(sy_call_t *)(sys_getpriority)) error_count++;
    if (sysent[SYS_bind].sy_call!=(sy_call_t *)(sys_bind)) error_count++;
    if (sysent[SYS_setsockopt].sy_call!=(sy_call_t *)(sys_setsockopt)) error_count++;
    if (sysent[SYS_listen].sy_call!=(sy_call_t *)(sys_listen)) error_count++;
    if (sysent[SYS_gettimeofday].sy_call!=(sy_call_t *)(sys_gettimeofday)) error_count++;
    if (sysent[SYS_getrusage].sy_call!=(sy_call_t *)(sys_getrusage)) error_count++;
    if (sysent[SYS_getsockopt].sy_call!=(sy_call_t *)(sys_getsockopt)) error_count++;
    if (sysent[SYS_readv].sy_call!=(sy_call_t *)(sys_readv)) error_count++;
    if (sysent[SYS_writev].sy_call!=(sy_call_t *)(sys_writev)) error_count++;
    if (sysent[SYS_settimeofday].sy_call!=(sy_call_t *)(sys_settimeofday)) error_count++;
    if (sysent[SYS_fchown].sy_call!=(sy_call_t *)(sys_fchown)) error_count++;
    if (sysent[SYS_fchmod].sy_call!=(sy_call_t *)(sys_fchmod)) error_count++;
    if (sysent[SYS_setreuid].sy_call!=(sy_call_t *)(sys_setreuid)) error_count++;
    if (sysent[SYS_setregid].sy_call!=(sy_call_t *)(sys_setregid)) error_count++;
    if (sysent[SYS_rename].sy_call!=(sy_call_t *)(sys_rename)) error_count++;
    if (sysent[SYS_flock].sy_call!=(sy_call_t *)(sys_flock)) error_count++;
    if (sysent[SYS_mkfifo].sy_call!=(sy_call_t *)(sys_mkfifo)) error_count++;
    if (sysent[SYS_sendto].sy_call!=(sy_call_t *)(sys_sendto)) error_count++;
    if (sysent[SYS_shutdown].sy_call!=(sy_call_t *)(sys_shutdown)) error_count++;
    if (sysent[SYS_socketpair].sy_call!=(sy_call_t *)(sys_socketpair)) error_count++;
    if (sysent[SYS_mkdir].sy_call!=(sy_call_t *)(sys_mkdir)) error_count++;
    if (sysent[SYS_rmdir].sy_call!=(sy_call_t *)(sys_rmdir)) error_count++;
    if (sysent[SYS_utimes].sy_call!=(sy_call_t *)(sys_utimes)) error_count++;
    if (sysent[SYS_adjtime].sy_call!=(sy_call_t *)(sys_adjtime)) error_count++;
    if (sysent[SYS_setsid].sy_call!=(sy_call_t *)(sys_setsid)) error_count++;
    if (sysent[SYS_quotactl].sy_call!=(sy_call_t *)(sys_quotactl)) error_count++;
    if (sysent[SYS_nlm_syscall].sy_call!=(sy_call_t *)(sys_nlm_syscall)) error_count++;
    if (sysent[SYS_nfssvc].sy_call!=(sy_call_t *)(sys_nfssvc)) error_count++;
    if (sysent[SYS_lgetfh].sy_call!=(sy_call_t *)(sys_lgetfh)) error_count++;
    if (sysent[SYS_getfh].sy_call!=(sy_call_t *)(sys_getfh)) error_count++;
    if (sysent[SYS_rtprio].sy_call!=(sy_call_t *)(sys_rtprio)) error_count++;
    if (sysent[SYS_semsys].sy_call!=(sy_call_t *)(sys_semsys)) error_count++;
    if (sysent[SYS_msgsys].sy_call!=(sy_call_t *)(sys_msgsys)) error_count++;
    if (sysent[SYS_setfib].sy_call!=(sy_call_t *)(sys_setfib)) error_count++;
    if (sysent[SYS_ntp_adjtime].sy_call!=(sy_call_t *)(sys_ntp_adjtime)) error_count++;
    if (sysent[SYS_setgid].sy_call!=(sy_call_t *)(sys_setgid)) error_count++;
    if (sysent[SYS_setegid].sy_call!=(sy_call_t *)(sys_setegid)) error_count++;
    if (sysent[SYS_seteuid].sy_call!=(sy_call_t *)(sys_seteuid)) error_count++;
    if (sysent[SYS_stat].sy_call!=(sy_call_t *)(sys_stat)) error_count++;
    if (sysent[SYS_fstat].sy_call!=(sy_call_t *)(sys_fstat)) error_count++;
    if (sysent[SYS_lstat].sy_call!=(sy_call_t *)(sys_lstat)) error_count++;
    if (sysent[SYS_pathconf].sy_call!=(sy_call_t *)(sys_pathconf)) error_count++;
    if (sysent[SYS_fpathconf].sy_call!=(sy_call_t *)(sys_fpathconf)) error_count++;
    if (sysent[SYS_getrlimit].sy_call!=(sy_call_t *)(sys_getrlimit)) error_count++;
    if (sysent[SYS_setrlimit].sy_call!=(sy_call_t *)(sys_setrlimit)) error_count++;
    if (sysent[SYS_getdirentries].sy_call!=(sy_call_t *)(sys_getdirentries)) error_count++;
    if (sysent[SYS___sysctl].sy_call!=(sy_call_t *)(sys___sysctl)) error_count++;
    if (sysent[SYS_mlock].sy_call!=(sy_call_t *)(sys_mlock)) error_count++;
    if (sysent[SYS_munlock].sy_call!=(sy_call_t *)(sys_munlock)) error_count++;
    if (sysent[SYS_undelete].sy_call!=(sy_call_t *)(sys_undelete)) error_count++;
    if (sysent[SYS_futimes].sy_call!=(sy_call_t *)(sys_futimes)) error_count++;
    if (sysent[SYS_getpgid].sy_call!=(sy_call_t *)(sys_getpgid)) error_count++;
    if (sysent[SYS_poll].sy_call!=(sy_call_t *)(sys_poll)) error_count++;
    if (sysent[SYS_semget].sy_call!=(sy_call_t *)(sys_semget)) error_count++;
    if (sysent[SYS_semop].sy_call!=(sy_call_t *)(sys_semop)) error_count++;
    if (sysent[SYS_msgget].sy_call!=(sy_call_t *)(sys_msgget)) error_count++;
    if (sysent[SYS_msgsnd].sy_call!=(sy_call_t *)(sys_msgsnd)) error_count++;
    if (sysent[SYS_msgrcv].sy_call!=(sy_call_t *)(sys_msgrcv)) error_count++;
    if (sysent[SYS_shmat].sy_call!=(sy_call_t *)(sys_shmat)) error_count++;
    if (sysent[SYS_shmdt].sy_call!=(sy_call_t *)(sys_shmdt)) error_count++;
    if (sysent[SYS_shmget].sy_call!=(sy_call_t *)(sys_shmget)) error_count++;
    if (sysent[SYS_clock_gettime].sy_call!=(sy_call_t *)(sys_clock_gettime)) error_count++;
    if (sysent[SYS_clock_settime].sy_call!=(sy_call_t *)(sys_clock_settime)) error_count++;
    if (sysent[SYS_clock_getres].sy_call!=(sy_call_t *)(sys_clock_getres)) error_count++;
    if (sysent[SYS_ktimer_create].sy_call!=(sy_call_t *)(sys_ktimer_create)) error_count++;
    if (sysent[SYS_ktimer_delete].sy_call!=(sy_call_t *)(sys_ktimer_delete)) error_count++;
    if (sysent[SYS_ktimer_settime].sy_call!=(sy_call_t *)(sys_ktimer_settime)) error_count++;
    if (sysent[SYS_ktimer_gettime].sy_call!=(sy_call_t *)(sys_ktimer_gettime)) error_count++;
    if (sysent[SYS_ktimer_getoverrun].sy_call!=(sy_call_t *)(sys_ktimer_getoverrun)) error_count++;
    if (sysent[SYS_nanosleep].sy_call!=(sy_call_t *)(sys_nanosleep)) error_count++;
    if (sysent[SYS_ffclock_getcounter].sy_call!=(sy_call_t *)(sys_ffclock_getcounter)) error_count++;
    if (sysent[SYS_ffclock_setestimate].sy_call!=(sy_call_t *)(sys_ffclock_setestimate)) error_count++;
    if (sysent[SYS_ffclock_getestimate].sy_call!=(sy_call_t *)(sys_ffclock_getestimate)) error_count++;
    if (sysent[SYS_clock_nanosleep].sy_call!=(sy_call_t *)(sys_clock_nanosleep)) error_count++;
    if (sysent[SYS_clock_getcpuclockid2].sy_call!=(sy_call_t *)(sys_clock_getcpuclockid2)) error_count++;
    if (sysent[SYS_ntp_gettime].sy_call!=(sy_call_t *)(sys_ntp_gettime)) error_count++;
    if (sysent[SYS_minherit].sy_call!=(sy_call_t *)(sys_minherit)) error_count++;
    if (sysent[SYS_rfork].sy_call!=(sy_call_t *)(sys_rfork)) error_count++;
    if (sysent[SYS_openbsd_poll].sy_call!=(sy_call_t *)(sys_openbsd_poll)) error_count++;
    if (sysent[SYS_issetugid].sy_call!=(sy_call_t *)(sys_issetugid)) error_count++;
    if (sysent[SYS_lchown].sy_call!=(sy_call_t *)(sys_lchown)) error_count++;
    if (sysent[SYS_aio_read].sy_call!=(sy_call_t *)(sys_aio_read)) error_count++;
    if (sysent[SYS_aio_write].sy_call!=(sy_call_t *)(sys_aio_write)) error_count++;
    if (sysent[SYS_lio_listio].sy_call!=(sy_call_t *)(sys_lio_listio)) error_count++;
    if (sysent[SYS_getdents].sy_call!=(sy_call_t *)(sys_getdents)) error_count++;
    if (sysent[SYS_lchmod].sy_call!=(sy_call_t *)(sys_lchmod)) error_count++;
    if (sysent[SYS_lutimes].sy_call!=(sy_call_t *)(sys_lutimes)) error_count++;
    if (sysent[SYS_nstat].sy_call!=(sy_call_t *)(sys_nstat)) error_count++;
    if (sysent[SYS_nfstat].sy_call!=(sy_call_t *)(sys_nfstat)) error_count++;
    if (sysent[SYS_nlstat].sy_call!=(sy_call_t *)(sys_nlstat)) error_count++;
    if (sysent[SYS_preadv].sy_call!=(sy_call_t *)(sys_preadv)) error_count++;
    if (sysent[SYS_pwritev].sy_call!=(sy_call_t *)(sys_pwritev)) error_count++;
    if (sysent[SYS_fhopen].sy_call!=(sy_call_t *)(sys_fhopen)) error_count++;
    if (sysent[SYS_fhstat].sy_call!=(sy_call_t *)(sys_fhstat)) error_count++;
    if (sysent[SYS_modnext].sy_call!=(sy_call_t *)(sys_modnext)) error_count++;
    if (sysent[SYS_modstat].sy_call!=(sy_call_t *)(sys_modstat)) error_count++;
    if (sysent[SYS_modfnext].sy_call!=(sy_call_t *)(sys_modfnext)) error_count++;
    if (sysent[SYS_modfind].sy_call!=(sy_call_t *)(sys_modfind)) error_count++;
    if (sysent[SYS_kldload].sy_call!=(sy_call_t *)(sys_kldload)) error_count++;
    if (sysent[SYS_kldunload].sy_call!=(sy_call_t *)(sys_kldunload)) error_count++;
    if (sysent[SYS_kldfind].sy_call!=(sy_call_t *)(sys_kldfind)) error_count++;
    if (sysent[SYS_kldnext].sy_call!=(sy_call_t *)(sys_kldnext)) error_count++;
    if (sysent[SYS_kldstat].sy_call!=(sy_call_t *)(sys_kldstat)) error_count++;
    if (sysent[SYS_kldfirstmod].sy_call!=(sy_call_t *)(sys_kldfirstmod)) error_count++;
    if (sysent[SYS_getsid].sy_call!=(sy_call_t *)(sys_getsid)) error_count++;
    if (sysent[SYS_setresuid].sy_call!=(sy_call_t *)(sys_setresuid)) error_count++;
    if (sysent[SYS_setresgid].sy_call!=(sy_call_t *)(sys_setresgid)) error_count++;
    if (sysent[SYS_aio_return].sy_call!=(sy_call_t *)(sys_aio_return)) error_count++;
    if (sysent[SYS_aio_suspend].sy_call!=(sy_call_t *)(sys_aio_suspend)) error_count++;
    if (sysent[SYS_aio_cancel].sy_call!=(sy_call_t *)(sys_aio_cancel)) error_count++;
    if (sysent[SYS_aio_error].sy_call!=(sy_call_t *)(sys_aio_error)) error_count++;
    if (sysent[SYS_yield].sy_call!=(sy_call_t *)(sys_yield)) error_count++;
    if (sysent[SYS_mlockall].sy_call!=(sy_call_t *)(sys_mlockall)) error_count++;
    if (sysent[SYS_munlockall].sy_call!=(sy_call_t *)(sys_munlockall)) error_count++;
    if (sysent[SYS___getcwd].sy_call!=(sy_call_t *)(sys___getcwd)) error_count++;
    if (sysent[SYS_sched_setparam].sy_call!=(sy_call_t *)(sys_sched_setparam)) error_count++;
    if (sysent[SYS_sched_getparam].sy_call!=(sy_call_t *)(sys_sched_getparam)) error_count++;
    if (sysent[SYS_sched_setscheduler].sy_call!=(sy_call_t *)(sys_sched_setscheduler)) error_count++;
    if (sysent[SYS_sched_getscheduler].sy_call!=(sy_call_t *)(sys_sched_getscheduler)) error_count++;
    if (sysent[SYS_sched_yield].sy_call!=(sy_call_t *)(sys_sched_yield)) error_count++;
    if (sysent[SYS_sched_get_priority_max].sy_call!=(sy_call_t *)(sys_sched_get_priority_max)) error_count++;
    if (sysent[SYS_sched_get_priority_min].sy_call!=(sy_call_t *)(sys_sched_get_priority_min)) error_count++;
    if (sysent[SYS_sched_rr_get_interval].sy_call!=(sy_call_t *)(sys_sched_rr_get_interval)) error_count++;
    if (sysent[SYS_utrace].sy_call!=(sy_call_t *)(sys_utrace)) error_count++;
    if (sysent[SYS_kldsym].sy_call!=(sy_call_t *)(sys_kldsym)) error_count++;
    if (sysent[SYS_jail].sy_call!=(sy_call_t *)(sys_jail)) error_count++;
    if (sysent[SYS_sigprocmask].sy_call!=(sy_call_t *)(sys_sigprocmask)) error_count++;
    if (sysent[SYS_sigsuspend].sy_call!=(sy_call_t *)(sys_sigsuspend)) error_count++;
    if (sysent[SYS_sigpending].sy_call!=(sy_call_t *)(sys_sigpending)) error_count++;
    if (sysent[SYS_sigtimedwait].sy_call!=(sy_call_t *)(sys_sigtimedwait)) error_count++;
    if (sysent[SYS_sigwaitinfo].sy_call!=(sy_call_t *)(sys_sigwaitinfo)) error_count++;
    if (sysent[SYS___acl_get_file].sy_call!=(sy_call_t *)(sys___acl_get_file)) error_count++;
    if (sysent[SYS___acl_set_file].sy_call!=(sy_call_t *)(sys___acl_set_file)) error_count++;
    if (sysent[SYS___acl_get_fd].sy_call!=(sy_call_t *)(sys___acl_get_fd)) error_count++;
    if (sysent[SYS___acl_set_fd].sy_call!=(sy_call_t *)(sys___acl_set_fd)) error_count++;
    if (sysent[SYS___acl_delete_file].sy_call!=(sy_call_t *)(sys___acl_delete_file)) error_count++;
    if (sysent[SYS___acl_delete_fd].sy_call!=(sy_call_t *)(sys___acl_delete_fd)) error_count++;
    if (sysent[SYS___acl_aclcheck_file].sy_call!=(sy_call_t *)(sys___acl_aclcheck_file)) error_count++;
    if (sysent[SYS___acl_aclcheck_fd].sy_call!=(sy_call_t *)(sys___acl_aclcheck_fd)) error_count++;
    if (sysent[SYS_extattrctl].sy_call!=(sy_call_t *)(sys_extattrctl)) error_count++;
    if (sysent[SYS_extattr_set_file].sy_call!=(sy_call_t *)(sys_extattr_set_file)) error_count++;
    if (sysent[SYS_extattr_get_file].sy_call!=(sy_call_t *)(sys_extattr_get_file)) error_count++;
    if (sysent[SYS_extattr_delete_file].sy_call!=(sy_call_t *)(sys_extattr_delete_file)) error_count++;
    if (sysent[SYS_aio_waitcomplete].sy_call!=(sy_call_t *)(sys_aio_waitcomplete)) error_count++;
    if (sysent[SYS_getresuid].sy_call!=(sy_call_t *)(sys_getresuid)) error_count++;
    if (sysent[SYS_getresgid].sy_call!=(sy_call_t *)(sys_getresgid)) error_count++;
    if (sysent[SYS_kqueue].sy_call!=(sy_call_t *)(sys_kqueue)) error_count++;
    if (sysent[SYS_kevent].sy_call!=(sy_call_t *)(sys_kevent)) error_count++;
    if (sysent[SYS_extattr_set_fd].sy_call!=(sy_call_t *)(sys_extattr_set_fd)) error_count++;
    if (sysent[SYS_extattr_get_fd].sy_call!=(sy_call_t *)(sys_extattr_get_fd)) error_count++;
    if (sysent[SYS_extattr_delete_fd].sy_call!=(sy_call_t *)(sys_extattr_delete_fd)) error_count++;
    if (sysent[SYS___setugid].sy_call!=(sy_call_t *)(sys___setugid)) error_count++;
    if (sysent[SYS_eaccess].sy_call!=(sy_call_t *)(sys_eaccess)) error_count++;
    if (sysent[SYS_nmount].sy_call!=(sy_call_t *)(sys_nmount)) error_count++;
    if (sysent[SYS___mac_get_proc].sy_call!=(sy_call_t *)(sys___mac_get_proc)) error_count++;
    if (sysent[SYS___mac_set_proc].sy_call!=(sy_call_t *)(sys___mac_set_proc)) error_count++;
    if (sysent[SYS___mac_get_fd].sy_call!=(sy_call_t *)(sys___mac_get_fd)) error_count++;
    if (sysent[SYS___mac_get_file].sy_call!=(sy_call_t *)(sys___mac_get_file)) error_count++;
    if (sysent[SYS___mac_set_fd].sy_call!=(sy_call_t *)(sys___mac_set_fd)) error_count++;
    if (sysent[SYS___mac_set_file].sy_call!=(sy_call_t *)(sys___mac_set_file)) error_count++;
    if (sysent[SYS_kenv].sy_call!=(sy_call_t *)(sys_kenv)) error_count++;
    if (sysent[SYS_lchflags].sy_call!=(sy_call_t *)(sys_lchflags)) error_count++;
    if (sysent[SYS_uuidgen].sy_call!=(sy_call_t *)(sys_uuidgen)) error_count++;
    if (sysent[SYS_sendfile].sy_call!=(sy_call_t *)(sys_sendfile)) error_count++;
    if (sysent[SYS_mac_syscall].sy_call!=(sy_call_t *)(sys_mac_syscall)) error_count++;
    if (sysent[SYS_getfsstat].sy_call!=(sy_call_t *)(sys_getfsstat)) error_count++;
    if (sysent[SYS_statfs].sy_call!=(sy_call_t *)(sys_statfs)) error_count++;
    if (sysent[SYS_fstatfs].sy_call!=(sy_call_t *)(sys_fstatfs)) error_count++;
    if (sysent[SYS_fhstatfs].sy_call!=(sy_call_t *)(sys_fhstatfs)) error_count++;
    if (sysent[SYS___mac_get_pid].sy_call!=(sy_call_t *)(sys___mac_get_pid)) error_count++;
    if (sysent[SYS___mac_get_link].sy_call!=(sy_call_t *)(sys___mac_get_link)) error_count++;
    if (sysent[SYS___mac_set_link].sy_call!=(sy_call_t *)(sys___mac_set_link)) error_count++;
    if (sysent[SYS_extattr_set_link].sy_call!=(sy_call_t *)(sys_extattr_set_link)) error_count++;
    if (sysent[SYS_extattr_get_link].sy_call!=(sy_call_t *)(sys_extattr_get_link)) error_count++;
    if (sysent[SYS_extattr_delete_link].sy_call!=(sy_call_t *)(sys_extattr_delete_link)) error_count++;
    if (sysent[SYS___mac_execve].sy_call!=(sy_call_t *)(sys___mac_execve)) error_count++;
    if (sysent[SYS_sigaction].sy_call!=(sy_call_t *)(sys_sigaction)) error_count++;
    if (sysent[SYS_sigreturn].sy_call!=(sy_call_t *)(sys_sigreturn)) error_count++;
    if (sysent[SYS_getcontext].sy_call!=(sy_call_t *)(sys_getcontext)) error_count++;
    if (sysent[SYS_setcontext].sy_call!=(sy_call_t *)(sys_setcontext)) error_count++;
    if (sysent[SYS_swapcontext].sy_call!=(sy_call_t *)(sys_swapcontext)) error_count++;
    if (sysent[SYS_swapoff].sy_call!=(sy_call_t *)(sys_swapoff)) error_count++;
    if (sysent[SYS___acl_get_link].sy_call!=(sy_call_t *)(sys___acl_get_link)) error_count++;
    if (sysent[SYS___acl_set_link].sy_call!=(sy_call_t *)(sys___acl_set_link)) error_count++;
    if (sysent[SYS___acl_delete_link].sy_call!=(sy_call_t *)(sys___acl_delete_link)) error_count++;
    if (sysent[SYS___acl_aclcheck_link].sy_call!=(sy_call_t *)(sys___acl_aclcheck_link)) error_count++;
    if (sysent[SYS_sigwait].sy_call!=(sy_call_t *)(sys_sigwait)) error_count++;
    if (sysent[SYS_thr_create].sy_call!=(sy_call_t *)(sys_thr_create)) error_count++;
    if (sysent[SYS_thr_exit].sy_call!=(sy_call_t *)(sys_thr_exit)) error_count++;
    if (sysent[SYS_thr_self].sy_call!=(sy_call_t *)(sys_thr_self)) error_count++;
    if (sysent[SYS_thr_kill].sy_call!=(sy_call_t *)(sys_thr_kill)) error_count++;
    if (sysent[SYS_jail_attach].sy_call!=(sy_call_t *)(sys_jail_attach)) error_count++;
    if (sysent[SYS_extattr_list_fd].sy_call!=(sy_call_t *)(sys_extattr_list_fd)) error_count++;
    if (sysent[SYS_extattr_list_file].sy_call!=(sy_call_t *)(sys_extattr_list_file)) error_count++;
    if (sysent[SYS_extattr_list_link].sy_call!=(sy_call_t *)(sys_extattr_list_link)) error_count++;
    if (sysent[SYS_thr_suspend].sy_call!=(sy_call_t *)(sys_thr_suspend)) error_count++;
    if (sysent[SYS_thr_wake].sy_call!=(sy_call_t *)(sys_thr_wake)) error_count++;
    if (sysent[SYS_kldunloadf].sy_call!=(sy_call_t *)(sys_kldunloadf)) error_count++;
    if (sysent[SYS_audit].sy_call!=(sy_call_t *)(sys_audit)) error_count++;
    if (sysent[SYS_auditon].sy_call!=(sy_call_t *)(sys_auditon)) error_count++;
    if (sysent[SYS_getauid].sy_call!=(sy_call_t *)(sys_getauid)) error_count++;
    if (sysent[SYS_setauid].sy_call!=(sy_call_t *)(sys_setauid)) error_count++;
    if (sysent[SYS_getaudit].sy_call!=(sy_call_t *)(sys_getaudit)) error_count++;
    if (sysent[SYS_setaudit].sy_call!=(sy_call_t *)(sys_setaudit)) error_count++;
    if (sysent[SYS_getaudit_addr].sy_call!=(sy_call_t *)(sys_getaudit_addr)) error_count++;
    if (sysent[SYS_setaudit_addr].sy_call!=(sy_call_t *)(sys_setaudit_addr)) error_count++;
    if (sysent[SYS_auditctl].sy_call!=(sy_call_t *)(sys_auditctl)) error_count++;
    if (sysent[SYS__umtx_op].sy_call!=(sy_call_t *)(sys__umtx_op)) error_count++;
    if (sysent[SYS_thr_new].sy_call!=(sy_call_t *)(sys_thr_new)) error_count++;
    if (sysent[SYS_sigqueue].sy_call!=(sy_call_t *)(sys_sigqueue)) error_count++;
    if (sysent[SYS_abort2].sy_call!=(sy_call_t *)(sys_abort2)) error_count++;
    if (sysent[SYS_thr_set_name].sy_call!=(sy_call_t *)(sys_thr_set_name)) error_count++;
    if (sysent[SYS_aio_fsync].sy_call!=(sy_call_t *)(sys_aio_fsync)) error_count++;
    if (sysent[SYS_rtprio_thread].sy_call!=(sy_call_t *)(sys_rtprio_thread)) error_count++;
    if (sysent[SYS_sctp_peeloff].sy_call!=(sy_call_t *)(sys_sctp_peeloff)) error_count++;
    if (sysent[SYS_sctp_generic_sendmsg].sy_call!=(sy_call_t *)(sys_sctp_generic_sendmsg)) error_count++;
    if (sysent[SYS_sctp_generic_sendmsg_iov].sy_call!=(sy_call_t *)(sys_sctp_generic_sendmsg_iov)) error_count++;
    if (sysent[SYS_sctp_generic_recvmsg].sy_call!=(sy_call_t *)(sys_sctp_generic_recvmsg)) error_count++;
    if (sysent[SYS_pread].sy_call!=(sy_call_t *)(sys_pread)) error_count++;
    if (sysent[SYS_pwrite].sy_call!=(sy_call_t *)(sys_pwrite)) error_count++;
    if (sysent[SYS_mmap].sy_call!=(sy_call_t *)(sys_mmap)) error_count++;
    if (sysent[SYS_lseek].sy_call!=(sy_call_t *)(sys_lseek)) error_count++;
    if (sysent[SYS_truncate].sy_call!=(sy_call_t *)(sys_truncate)) error_count++;
    if (sysent[SYS_ftruncate].sy_call!=(sy_call_t *)(sys_ftruncate)) error_count++;
    if (sysent[SYS_thr_kill2].sy_call!=(sy_call_t *)(sys_thr_kill2)) error_count++;
    if (sysent[SYS_shm_open].sy_call!=(sy_call_t *)(sys_shm_open)) error_count++;
    if (sysent[SYS_shm_unlink].sy_call!=(sy_call_t *)(sys_shm_unlink)) error_count++;
    if (sysent[SYS_cpuset].sy_call!=(sy_call_t *)(sys_cpuset)) error_count++;
    if (sysent[SYS_cpuset_setid].sy_call!=(sy_call_t *)(sys_cpuset_setid)) error_count++;
    if (sysent[SYS_cpuset_getid].sy_call!=(sy_call_t *)(sys_cpuset_getid)) error_count++;
    if (sysent[SYS_cpuset_getaffinity].sy_call!=(sy_call_t *)(sys_cpuset_getaffinity)) error_count++;
    if (sysent[SYS_cpuset_setaffinity].sy_call!=(sy_call_t *)(sys_cpuset_setaffinity)) error_count++;
    if (sysent[SYS_faccessat].sy_call!=(sy_call_t *)(sys_faccessat)) error_count++;
    if (sysent[SYS_fchmodat].sy_call!=(sy_call_t *)(sys_fchmodat)) error_count++;
    if (sysent[SYS_fchownat].sy_call!=(sy_call_t *)(sys_fchownat)) error_count++;
    if (sysent[SYS_fexecve].sy_call!=(sy_call_t *)(sys_fexecve)) error_count++;
    if (sysent[SYS_fstatat].sy_call!=(sy_call_t *)(sys_fstatat)) error_count++;
    if (sysent[SYS_futimesat].sy_call!=(sy_call_t *)(sys_futimesat)) error_count++;
    if (sysent[SYS_linkat].sy_call!=(sy_call_t *)(sys_linkat)) error_count++;
    if (sysent[SYS_mkdirat].sy_call!=(sy_call_t *)(sys_mkdirat)) error_count++;
    if (sysent[SYS_mkfifoat].sy_call!=(sy_call_t *)(sys_mkfifoat)) error_count++;
    if (sysent[SYS_mknodat].sy_call!=(sy_call_t *)(sys_mknodat)) error_count++;
    if (sysent[SYS_openat].sy_call!=(sy_call_t *)(sys_openat)) error_count++;
    if (sysent[SYS_readlinkat].sy_call!=(sy_call_t *)(sys_readlinkat)) error_count++;
    if (sysent[SYS_renameat].sy_call!=(sy_call_t *)(sys_renameat)) error_count++;
    if (sysent[SYS_symlinkat].sy_call!=(sy_call_t *)(sys_symlinkat)) error_count++;
    if (sysent[SYS_unlinkat].sy_call!=(sy_call_t *)(sys_unlinkat)) error_count++;
    if (sysent[SYS_posix_openpt].sy_call!=(sy_call_t *)(sys_posix_openpt)) error_count++;
    if (sysent[SYS_jail_get].sy_call!=(sy_call_t *)(sys_jail_get)) error_count++;
    if (sysent[SYS_jail_set].sy_call!=(sy_call_t *)(sys_jail_set)) error_count++;
    if (sysent[SYS_jail_remove].sy_call!=(sy_call_t *)(sys_jail_remove)) error_count++;
    if (sysent[SYS_closefrom].sy_call!=(sy_call_t *)(sys_closefrom)) error_count++;
    if (sysent[SYS___semctl].sy_call!=(sy_call_t *)(sys___semctl)) error_count++;
    if (sysent[SYS_msgctl].sy_call!=(sy_call_t *)(sys_msgctl)) error_count++;
    if (sysent[SYS_shmctl].sy_call!=(sy_call_t *)(sys_shmctl)) error_count++;
    if (sysent[SYS_lpathconf].sy_call!=(sy_call_t *)(sys_lpathconf)) error_count++;
    if (sysent[SYS___cap_rights_get].sy_call!=(sy_call_t *)(sys___cap_rights_get)) error_count++;
    if (sysent[SYS_cap_enter].sy_call!=(sy_call_t *)(sys_cap_enter)) error_count++;
    if (sysent[SYS_cap_getmode].sy_call!=(sy_call_t *)(sys_cap_getmode)) error_count++;
    if (sysent[SYS_pdfork].sy_call!=(sy_call_t *)(sys_pdfork)) error_count++;
    if (sysent[SYS_pdkill].sy_call!=(sy_call_t *)(sys_pdkill)) error_count++;
    if (sysent[SYS_pdgetpid].sy_call!=(sy_call_t *)(sys_pdgetpid)) error_count++;
    if (sysent[SYS_pselect].sy_call!=(sy_call_t *)(sys_pselect)) error_count++;
    if (sysent[SYS_getloginclass].sy_call!=(sy_call_t *)(sys_getloginclass)) error_count++;
    if (sysent[SYS_setloginclass].sy_call!=(sy_call_t *)(sys_setloginclass)) error_count++;
    if (sysent[SYS_rctl_get_racct].sy_call!=(sy_call_t *)(sys_rctl_get_racct)) error_count++;
    if (sysent[SYS_rctl_get_rules].sy_call!=(sy_call_t *)(sys_rctl_get_rules)) error_count++;
    if (sysent[SYS_rctl_get_limits].sy_call!=(sy_call_t *)(sys_rctl_get_limits)) error_count++;
    if (sysent[SYS_rctl_add_rule].sy_call!=(sy_call_t *)(sys_rctl_add_rule)) error_count++;
    if (sysent[SYS_rctl_remove_rule].sy_call!=(sy_call_t *)(sys_rctl_remove_rule)) error_count++;
    if (sysent[SYS_posix_fallocate].sy_call!=(sy_call_t *)(sys_posix_fallocate)) error_count++;
    if (sysent[SYS_posix_fadvise].sy_call!=(sy_call_t *)(sys_posix_fadvise)) error_count++;
    if (sysent[SYS_wait6].sy_call!=(sy_call_t *)(sys_wait6)) error_count++;
    if (sysent[SYS_cap_rights_limit].sy_call!=(sy_call_t *)(sys_cap_rights_limit)) error_count++;
    if (sysent[SYS_cap_ioctls_limit].sy_call!=(sy_call_t *)(sys_cap_ioctls_limit)) error_count++;
    if (sysent[SYS_cap_ioctls_get].sy_call!=(sy_call_t *)(sys_cap_ioctls_get)) error_count++;
    if (sysent[SYS_cap_fcntls_limit].sy_call!=(sy_call_t *)(sys_cap_fcntls_limit)) error_count++;
    if (sysent[SYS_cap_fcntls_get].sy_call!=(sy_call_t *)(sys_cap_fcntls_get)) error_count++;
    if (sysent[SYS_bindat].sy_call!=(sy_call_t *)(sys_bindat)) error_count++;
    if (sysent[SYS_connectat].sy_call!=(sy_call_t *)(sys_connectat)) error_count++;
    if (sysent[SYS_chflagsat].sy_call!=(sy_call_t *)(sys_chflagsat)) error_count++;
    if (sysent[SYS_accept4].sy_call!=(sy_call_t *)(sys_accept4)) error_count++;
    if (sysent[SYS_pipe2].sy_call!=(sy_call_t *)(sys_pipe2)) error_count++;
    if (sysent[SYS_aio_mlock].sy_call!=(sy_call_t *)(sys_aio_mlock)) error_count++;
    if (sysent[SYS_procctl].sy_call!=(sy_call_t *)(sys_procctl)) error_count++;
    if (sysent[SYS_ppoll].sy_call!=(sy_call_t *)(sys_ppoll)) error_count++;
    if (sysent[SYS_futimens].sy_call!=(sy_call_t *)(sys_futimens)) error_count++;
    if (sysent[SYS_utimensat].sy_call!=(sy_call_t *)(sys_utimensat)) error_count++;
    if (sysent[SYS_numa_getaffinity].sy_call!=(sy_call_t *)(sys_numa_getaffinity)) error_count++;
    if (sysent[SYS_numa_setaffinity].sy_call!=(sy_call_t *)(sys_numa_setaffinity)) error_count++;
    if (sysent[SYS_fdatasync].sy_call!=(sy_call_t *)(sys_fdatasync)) error_count++;

    if (error_count != 0) return 1;

    for(int i=0; i<SYS_MAXSYSCALL; i++)
        if (is_empty_syscall(i)
            && sysent[i].sy_call != (sy_call_t *)nosys
            && sysent[i].sy_call != (sy_call_t *)lkmnosys
            && i != sda->to_ignore)
        return 1;

    return 0;
}

static struct sysent sys_detect_mod = {
    1,            /* sy_narg */
    (sy_call_t *)sys_detect_handler        /* sy_call */
};

static int offset = 210;

static int
load (struct module *module, int cmd, void *arg)
{
    int error = 0;

    switch (cmd) {
        case MOD_LOAD:
        uprintf("System call loaded at offset %.d\n", offset);
        break;

        case MOD_UNLOAD:
        uprintf("System call unloaded from offset %d.\n", offset);
        break;

        default:
        error = EOPNOTSUPP;
        break;
    }

    return(error);
}

// DECLARE_MODULE(syscall, syscall_mod, SI_SUB_DRIVERS, SI_ORDER_MIDDLE);
SYSCALL_MODULE(rootmod, &offset, &sys_detect_mod, load, NULL);
