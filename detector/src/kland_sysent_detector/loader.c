#include <sys/param.h>
#include <sys/linker.h>

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define FILE_TO_LOAD "./binaries/kernel_checker.ko"

struct ags {
	int ignore;
};

int main(int argc, char **argv){
	errno = 0;
	int i = kldload(FILE_TO_LOAD);
	if (errno != 0 && errno != ENOENT && errno != ENOEXEC) {
		puts("Something fishy...\n");
		return 1; // something
	}
	struct ags a;
	a.ignore = 210;
	i = syscall(210,a.ignore);
	printf("Checking\n");
	if (i == 1) {
		printf("We found something");
		return 1;

	}
	system("kldunload kernel_checker.ko");
	return 0;
}
