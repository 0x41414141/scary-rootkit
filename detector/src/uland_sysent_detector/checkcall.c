#include <fcntl.h>
#include <kvm.h>
#include <limits.h>
#include <nlist.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <sys/param.h>
#include <sys/sysent.h>
#include <sys/sysproto.h>
#include <sys/types.h>

#define SYS_MAXSYSCALL 551
#define LEN_BLIST 22

void usage();
bool is_hooked(int entry_num, char *name, bool is_normally_empty);
int get_list();
bool is_blacklisted(int num);

int main(int argc, char **argv) {
	int i = get_list();
	printf("Returning %d\n",i);
	return(i);
}

int blacklist[LEN_BLIST] = {18,156,157,158, 162,163,164,173,174, 197,199,200,201,297, 318, 319, 320, 336, 342, 344, 1, 17};
sy_call_t * first_null = NULL;
sy_call_t * second_null = NULL;

bool is_blacklisted(int num) {
	for(int i=0; i<LEN_BLIST; i++)
		if (blacklist[i] == num)
			return true;

	return false;
}


bool is_hooked(int entry_num, char *name, bool is_normally_empty) {
	char errbuff[_POSIX2_LINE_MAX];
	kvm_t *kd;
	struct nlist nl[] = { {NULL}, {NULL}, {NULL},  };

	unsigned long addr;
	int callnum;
	struct sysent call;

	nl[0].n_name = "sysent";
	nl[1].n_name = name;

//callnum = (int) strtol(entry_num, (char**)NULL, 10);
	callnum = entry_num;

	printf("Checking system call %d: %s\n\n", callnum, name);

	kd = kvm_openfiles(NULL, NULL, NULL, O_RDWR, errbuff);
	if (!kd) {
		fprintf(stderr, "ERROR: %s\n", errbuff);
		return 0;
	}

	if (kvm_nlist(kd, nl) < 0) {
		fprintf(stderr, "ERROR: %s\n", kvm_geterr(kd));
		return 0;
	}

	if (nl[0].n_value) {
		printf("%s[] is 0x%x at 0x%1x\n",
			nl[0].n_name, nl[0].n_type, nl[0].n_value);
	} else {
		fprintf(stderr, "ERROR: %s not found (very weird...)\n",
				nl[0].n_name);
		return 0;
	}

	if (!nl[1].n_value && name != NULL) {
		fprintf(stderr, "ERROR: %s not found\n", nl[1].n_name);
		return 0;
	}

	addr = nl[0].n_value + callnum * sizeof(struct sysent);

	if (kvm_read(kd, addr, &call, sizeof(struct sysent)) < 0) {
		fprintf(stderr, "ERROR: %s\n", kvm_geterr(kd));
		return 0;
	}

	printf("sysent[%d] is at 0x%lx and its sy_call member points %p\n"
			, callnum, addr, call.sy_call);

	if (is_normally_empty) {
		if (first_null == NULL) {
			first_null = call.sy_call;
			return 0;
		}

		if (second_null == NULL && call.sy_call != first_null) {
			second_null = call.sy_call;
			return 0;
		}


		if (first_null != call.sy_call && second_null != call.sy_call) // hijacked syscall
			return true;

		return false;
	}

	if ((uintptr_t)call.sy_call != nl[1].n_value) {
		printf("ALERT! It should point to 0x%lx instead \n"
				, nl[1].n_value);
		return 1;

		/*if (argv[3] && strncmp(argv[3], "fix", 3) == 0) {
			printf("Fixing it...");

			call.sy_call = (sy_call_t*)(uintptr_t)nl[1].n_value;
			if (kvm_write(kd, addr, &call, sizeof(struct sysent)) < 0) {
				fprintf(stderr, "ERROR: %s\n", kvm_geterr(kd));
				exit(-1);
			}

			printf("DONE.\n");
		}*/
	}

	if (kvm_close(kd) < 0) {
		fprintf(stderr, "ERROR: %s\n", kvm_geterr(kd));
		return 0;
	}

	return 0;
}

void
usage() {
	fprintf(stderr, "Usage:\ncheckcall [system call function] [call number] <fix>\n\n");
	fprintf(stderr, "For a list of system call numbers see /sys/sys/syscall.h\n");
}

void lower_string(char *p) {
	for ( ; *p; ++p) *p = tolower(*p);
}

int get_list() {
	char *sysent_list[SYS_MAXSYSCALL];
	for(int i = 0; i < SYS_MAXSYSCALL; i++)
		sysent_list[i] = NULL;
    sysent_list[0] = "sys_syscall";
    //sysent_list[1] = "sys_exit";
    sysent_list[2] = "sys_fork";
    sysent_list[3] = "sys_read";
    sysent_list[4] = "sys_write";
    sysent_list[5] = "sys_open";
    sysent_list[6] = "sys_close";
    sysent_list[7] = "sys_wait4";
    sysent_list[9] = "sys_link";
    sysent_list[10] = "sys_unlink";
    sysent_list[12] = "sys_chdir";
    sysent_list[13] = "sys_fchdir";
    sysent_list[14] = "sys_mknod";
    sysent_list[15] = "sys_chmod";
    sysent_list[16] = "sys_chown";
    //sysent_list[17] = "sys_break";
    sysent_list[20] = "sys_getpid";
    sysent_list[21] = "sys_mount";
    sysent_list[22] = "sys_unmount";
    sysent_list[23] = "sys_setuid";
    sysent_list[24] = "sys_getuid";
    sysent_list[25] = "sys_geteuid";
    sysent_list[26] = "sys_ptrace";
    sysent_list[27] = "sys_recvmsg";
    sysent_list[28] = "sys_sendmsg";
    sysent_list[29] = "sys_recvfrom";
    sysent_list[30] = "sys_accept";
    sysent_list[31] = "sys_getpeername";
    sysent_list[32] = "sys_getsockname";
    sysent_list[33] = "sys_access";
    sysent_list[34] = "sys_chflags";
    sysent_list[35] = "sys_fchflags";
    sysent_list[36] = "sys_sync";
    sysent_list[37] = "sys_kill";
    sysent_list[39] = "sys_getppid";
    sysent_list[41] = "sys_dup";
    sysent_list[42] = "sys_freebsd10_pipe";
    sysent_list[43] = "sys_getegid";
    sysent_list[44] = "sys_profil";
    sysent_list[45] = "sys_ktrace";
    sysent_list[47] = "sys_getgid";
    sysent_list[49] = "sys_getlogin";
    sysent_list[50] = "sys_setlogin";
    sysent_list[51] = "sys_acct";
    sysent_list[53] = "sys_sigaltstack";
    sysent_list[54] = "sys_ioctl";
    sysent_list[55] = "sys_reboot";
    sysent_list[56] = "sys_revoke";
    sysent_list[57] = "sys_symlink";
    sysent_list[58] = "sys_readlink";
    sysent_list[59] = "sys_execve";
    sysent_list[60] = "sys_umask";
    sysent_list[61] = "sys_chroot";
    sysent_list[65] = "sys_msync";
    sysent_list[66] = "sys_vfork";
    sysent_list[69] = "sys_sbrk";
    sysent_list[70] = "sys_sstk";
    sysent_list[72] = "sys_vadvise";
    sysent_list[73] = "sys_munmap";
    sysent_list[74] = "sys_mprotect";
    sysent_list[75] = "sys_madvise";
    sysent_list[78] = "sys_mincore";
    sysent_list[79] = "sys_getgroups";
    sysent_list[80] = "sys_setgroups";
    sysent_list[81] = "sys_getpgrp";
    sysent_list[82] = "sys_setpgid";
    sysent_list[83] = "sys_setitimer";
    sysent_list[85] = "sys_swapon";
    sysent_list[86] = "sys_getitimer";
    sysent_list[89] = "sys_getdtablesize";
    sysent_list[90] = "sys_dup2";
    sysent_list[92] = "sys_fcntl";
    sysent_list[93] = "sys_select";
    sysent_list[95] = "sys_fsync";
    sysent_list[96] = "sys_setpriority";
    sysent_list[97] = "sys_socket";
    sysent_list[98] = "sys_connect";
    sysent_list[100] = "sys_getpriority";
    sysent_list[104] = "sys_bind";
    sysent_list[105] = "sys_setsockopt";
    sysent_list[106] = "sys_listen";
    sysent_list[116] = "sys_gettimeofday";
    sysent_list[117] = "sys_getrusage";
    sysent_list[118] = "sys_getsockopt";
    sysent_list[120] = "sys_readv";
    sysent_list[121] = "sys_writev";
    sysent_list[122] = "sys_settimeofday";
    sysent_list[123] = "sys_fchown";
    sysent_list[124] = "sys_fchmod";
    sysent_list[126] = "sys_setreuid";
    sysent_list[127] = "sys_setregid";
    sysent_list[128] = "sys_rename";
    sysent_list[131] = "sys_flock";
    sysent_list[132] = "sys_mkfifo";
    sysent_list[133] = "sys_sendto";
    sysent_list[134] = "sys_shutdown";
    sysent_list[135] = "sys_socketpair";
    sysent_list[136] = "sys_mkdir";
    sysent_list[137] = "sys_rmdir";
    sysent_list[138] = "sys_utimes";
    sysent_list[140] = "sys_adjtime";
    sysent_list[147] = "sys_setsid";
    sysent_list[148] = "sys_quotactl";
    sysent_list[154] = "sys_nlm_syscall";
    sysent_list[155] = "sys_nfssvc";
    sysent_list[160] = "sys_lgetfh";
    sysent_list[161] = "sys_getfh";
    sysent_list[165] = "sys_sysarch";
    sysent_list[166] = "sys_rtprio";
    sysent_list[169] = "sys_semsys";
    sysent_list[170] = "sys_msgsys";
    sysent_list[171] = "sys_shmsys";
    sysent_list[175] = "sys_setfib";
    sysent_list[176] = "sys_ntp_adjtime";
    sysent_list[181] = "sys_setgid";
    sysent_list[182] = "sys_setegid";
    sysent_list[183] = "sys_seteuid";
    sysent_list[188] = "sys_stat";
    sysent_list[189] = "sys_fstat";
    sysent_list[190] = "sys_lstat";
    sysent_list[191] = "sys_pathconf";
    sysent_list[192] = "sys_fpathconf";
    sysent_list[194] = "sys_getrlimit";
    sysent_list[195] = "sys_setrlimit";
    sysent_list[196] = "sys_getdirentries";
    sysent_list[198] = "sys___syscall";
    sysent_list[202] = "sys___sysctl";
    sysent_list[203] = "sys_mlock";
    sysent_list[204] = "sys_munlock";
    sysent_list[205] = "sys_undelete";
    sysent_list[206] = "sys_futimes";
    sysent_list[207] = "sys_getpgid";
    sysent_list[209] = "sys_poll";
    sysent_list[220] = "sys_freebsd7___semctl";
    sysent_list[221] = "sys_semget";
    sysent_list[222] = "sys_semop";
    sysent_list[224] = "sys_freebsd7_msgctl";
    sysent_list[225] = "sys_msgget";
    sysent_list[226] = "sys_msgsnd";
    sysent_list[227] = "sys_msgrcv";
    sysent_list[228] = "sys_shmat";
    sysent_list[229] = "sys_freebsd7_shmctl";
    sysent_list[230] = "sys_shmdt";
    sysent_list[231] = "sys_shmget";
    sysent_list[232] = "sys_clock_gettime";
    sysent_list[233] = "sys_clock_settime";
    sysent_list[234] = "sys_clock_getres";
    sysent_list[235] = "sys_ktimer_create";
    sysent_list[236] = "sys_ktimer_delete";
    sysent_list[237] = "sys_ktimer_settime";
    sysent_list[238] = "sys_ktimer_gettime";
    sysent_list[239] = "sys_ktimer_getoverrun";
    sysent_list[240] = "sys_nanosleep";
    sysent_list[241] = "sys_ffclock_getcounter";
    sysent_list[242] = "sys_ffclock_setestimate";
    sysent_list[243] = "sys_ffclock_getestimate";
    sysent_list[244] = "sys_clock_nanosleep";
    sysent_list[247] = "sys_clock_getcpuclockid2";
    sysent_list[248] = "sys_ntp_gettime";
    sysent_list[250] = "sys_minherit";
    sysent_list[251] = "sys_rfork";
    sysent_list[252] = "sys_openbsd_poll";
    sysent_list[253] = "sys_issetugid";
    sysent_list[254] = "sys_lchown";
    sysent_list[255] = "sys_aio_read";
    sysent_list[256] = "sys_aio_write";
    sysent_list[257] = "sys_lio_listio";
    sysent_list[272] = "sys_getdents";
    sysent_list[274] = "sys_lchmod";
    sysent_list[275] = "sys_netbsd_lchown";
    sysent_list[276] = "sys_lutimes";
    sysent_list[277] = "sys_netbsd_msync";
    sysent_list[278] = "sys_nstat";
    sysent_list[279] = "sys_nfstat";
    sysent_list[280] = "sys_nlstat";
    sysent_list[289] = "sys_preadv";
    sysent_list[290] = "sys_pwritev";
    sysent_list[298] = "sys_fhopen";
    sysent_list[299] = "sys_fhstat";
    sysent_list[300] = "sys_modnext";
    sysent_list[301] = "sys_modstat";
    sysent_list[302] = "sys_modfnext";
    sysent_list[303] = "sys_modfind";
    sysent_list[304] = "sys_kldload";
    sysent_list[305] = "sys_kldunload";
    sysent_list[306] = "sys_kldfind";
    sysent_list[307] = "sys_kldnext";
    sysent_list[308] = "sys_kldstat";
    sysent_list[309] = "sys_kldfirstmod";
    sysent_list[310] = "sys_getsid";
    sysent_list[311] = "sys_setresuid";
    sysent_list[312] = "sys_setresgid";
    sysent_list[314] = "sys_aio_return";
    sysent_list[315] = "sys_aio_suspend";
    sysent_list[316] = "sys_aio_cancel";
    sysent_list[317] = "sys_aio_error";
    sysent_list[321] = "sys_yield";
    sysent_list[324] = "sys_mlockall";
    sysent_list[325] = "sys_munlockall";
    sysent_list[326] = "sys___getcwd";
    sysent_list[327] = "sys_sched_setparam";
    sysent_list[328] = "sys_sched_getparam";
    sysent_list[329] = "sys_sched_setscheduler";
    sysent_list[330] = "sys_sched_getscheduler";
    sysent_list[331] = "sys_sched_yield";
    sysent_list[332] = "sys_sched_get_priority_max";
    sysent_list[333] = "sys_sched_get_priority_min";
    sysent_list[334] = "sys_sched_rr_get_interval";
    sysent_list[335] = "sys_utrace";
    sysent_list[337] = "sys_kldsym";
    sysent_list[338] = "sys_jail";
    sysent_list[339] = "sys_nnpfs_syscall";
    sysent_list[340] = "sys_sigprocmask";
    sysent_list[341] = "sys_sigsuspend";
    sysent_list[343] = "sys_sigpending";
    sysent_list[345] = "sys_sigtimedwait";
    sysent_list[346] = "sys_sigwaitinfo";
    sysent_list[347] = "sys___acl_get_file";
    sysent_list[348] = "sys___acl_set_file";
    sysent_list[349] = "sys___acl_get_fd";
    sysent_list[350] = "sys___acl_set_fd";
    sysent_list[351] = "sys___acl_delete_file";
    sysent_list[352] = "sys___acl_delete_fd";
    sysent_list[353] = "sys___acl_aclcheck_file";
    sysent_list[354] = "sys___acl_aclcheck_fd";
    sysent_list[355] = "sys_extattrctl";
    sysent_list[356] = "sys_extattr_set_file";
    sysent_list[357] = "sys_extattr_get_file";
    sysent_list[358] = "sys_extattr_delete_file";
    sysent_list[359] = "sys_aio_waitcomplete";
    sysent_list[360] = "sys_getresuid";
    sysent_list[361] = "sys_getresgid";
    sysent_list[362] = "sys_kqueue";
    sysent_list[363] = "sys_kevent";
    sysent_list[371] = "sys_extattr_set_fd";
    sysent_list[372] = "sys_extattr_get_fd";
    sysent_list[373] = "sys_extattr_delete_fd";
    sysent_list[374] = "sys___setugid";
    sysent_list[376] = "sys_eaccess";
    sysent_list[377] = "sys_afs3_syscall";
    sysent_list[378] = "sys_nmount";
    sysent_list[384] = "sys___mac_get_proc";
    sysent_list[385] = "sys___mac_set_proc";
    sysent_list[386] = "sys___mac_get_fd";
    sysent_list[387] = "sys___mac_get_file";
    sysent_list[388] = "sys___mac_set_fd";
    sysent_list[389] = "sys___mac_set_file";
    sysent_list[390] = "sys_kenv";
    sysent_list[391] = "sys_lchflags";
    sysent_list[392] = "sys_uuidgen";
    sysent_list[393] = "sys_sendfile";
    sysent_list[394] = "sys_mac_syscall";
    sysent_list[395] = "sys_getfsstat";
    sysent_list[396] = "sys_statfs";
    sysent_list[397] = "sys_fstatfs";
    sysent_list[398] = "sys_fhstatfs";
    sysent_list[400] = "sys_ksem_close";
    sysent_list[401] = "sys_ksem_post";
    sysent_list[402] = "sys_ksem_wait";
    sysent_list[403] = "sys_ksem_trywait";
    sysent_list[404] = "sys_ksem_init";
    sysent_list[405] = "sys_ksem_open";
    sysent_list[406] = "sys_ksem_unlink";
    sysent_list[407] = "sys_ksem_getvalue";
    sysent_list[408] = "sys_ksem_destroy";
    sysent_list[409] = "sys___mac_get_pid";
    sysent_list[410] = "sys___mac_get_link";
    sysent_list[411] = "sys___mac_set_link";
    sysent_list[412] = "sys_extattr_set_link";
    sysent_list[413] = "sys_extattr_get_link";
    sysent_list[414] = "sys_extattr_delete_link";
    sysent_list[415] = "sys___mac_execve";
    sysent_list[416] = "sys_sigaction";
    sysent_list[417] = "sys_sigreturn";
    sysent_list[421] = "sys_getcontext";
    sysent_list[422] = "sys_setcontext";
    sysent_list[423] = "sys_swapcontext";
    sysent_list[424] = "sys_swapoff";
    sysent_list[425] = "sys___acl_get_link";
    sysent_list[426] = "sys___acl_set_link";
    sysent_list[427] = "sys___acl_delete_link";
    sysent_list[428] = "sys___acl_aclcheck_link";
    sysent_list[429] = "sys_sigwait";
    sysent_list[430] = "sys_thr_create";
    sysent_list[431] = "sys_thr_exit";
    sysent_list[432] = "sys_thr_self";
    sysent_list[433] = "sys_thr_kill";
    sysent_list[436] = "sys_jail_attach";
    sysent_list[437] = "sys_extattr_list_fd";
    sysent_list[438] = "sys_extattr_list_file";
    sysent_list[439] = "sys_extattr_list_link";
    sysent_list[441] = "sys_ksem_timedwait";
    sysent_list[442] = "sys_thr_suspend";
    sysent_list[443] = "sys_thr_wake";
    sysent_list[444] = "sys_kldunloadf";
    sysent_list[445] = "sys_audit";
    sysent_list[446] = "sys_auditon";
    sysent_list[447] = "sys_getauid";
    sysent_list[448] = "sys_setauid";
    sysent_list[449] = "sys_getaudit";
    sysent_list[450] = "sys_setaudit";
    sysent_list[451] = "sys_getaudit_addr";
    sysent_list[452] = "sys_setaudit_addr";
    sysent_list[453] = "sys_auditctl";
    sysent_list[454] = "sys__umtx_op";
    sysent_list[455] = "sys_thr_new";
    sysent_list[456] = "sys_sigqueue";
    sysent_list[457] = "sys_kmq_open";
    sysent_list[458] = "sys_kmq_setattr";
    sysent_list[459] = "sys_kmq_timedreceive";
    sysent_list[460] = "sys_kmq_timedsend";
    sysent_list[461] = "sys_kmq_notify";
    sysent_list[462] = "sys_kmq_unlink";
    sysent_list[463] = "sys_abort2";
    sysent_list[464] = "sys_thr_set_name";
    sysent_list[465] = "sys_aio_fsync";
    sysent_list[466] = "sys_rtprio_thread";
    sysent_list[471] = "sys_sctp_peeloff";
    sysent_list[472] = "sys_sctp_generic_sendmsg";
    sysent_list[473] = "sys_sctp_generic_sendmsg_iov";
    sysent_list[474] = "sys_sctp_generic_recvmsg";
    sysent_list[475] = "sys_pread";
    sysent_list[476] = "sys_pwrite";
    sysent_list[477] = "sys_mmap";
    sysent_list[478] = "sys_lseek";
    sysent_list[479] = "sys_truncate";
    sysent_list[480] = "sys_ftruncate";
    sysent_list[481] = "sys_thr_kill2";
    sysent_list[482] = "sys_shm_open";
    sysent_list[483] = "sys_shm_unlink";
    sysent_list[484] = "sys_cpuset";
    sysent_list[485] = "sys_cpuset_setid";
    sysent_list[486] = "sys_cpuset_getid";
    sysent_list[487] = "sys_cpuset_getaffinity";
    sysent_list[488] = "sys_cpuset_setaffinity";
    sysent_list[489] = "sys_faccessat";
    sysent_list[490] = "sys_fchmodat";
    sysent_list[491] = "sys_fchownat";
    sysent_list[492] = "sys_fexecve";
    sysent_list[493] = "sys_fstatat";
    sysent_list[494] = "sys_futimesat";
    sysent_list[495] = "sys_linkat";
    sysent_list[496] = "sys_mkdirat";
    sysent_list[497] = "sys_mkfifoat";
    sysent_list[498] = "sys_mknodat";
    sysent_list[499] = "sys_openat";
    sysent_list[500] = "sys_readlinkat";
    sysent_list[501] = "sys_renameat";
    sysent_list[502] = "sys_symlinkat";
    sysent_list[503] = "sys_unlinkat";
    sysent_list[504] = "sys_posix_openpt";
    sysent_list[505] = "sys_gssd_syscall";
    sysent_list[506] = "sys_jail_get";
    sysent_list[507] = "sys_jail_set";
    sysent_list[508] = "sys_jail_remove";
    sysent_list[509] = "sys_closefrom";
    sysent_list[510] = "sys___semctl";
    sysent_list[511] = "sys_msgctl";
    sysent_list[512] = "sys_shmctl";
    sysent_list[513] = "sys_lpathconf";
    sysent_list[515] = "sys___cap_rights_get";
    sysent_list[516] = "sys_cap_enter";
    sysent_list[517] = "sys_cap_getmode";
    sysent_list[518] = "sys_pdfork";
    sysent_list[519] = "sys_pdkill";
    sysent_list[520] = "sys_pdgetpid";
    sysent_list[522] = "sys_pselect";
    sysent_list[523] = "sys_getloginclass";
    sysent_list[524] = "sys_setloginclass";
    sysent_list[525] = "sys_rctl_get_racct";
    sysent_list[526] = "sys_rctl_get_rules";
    sysent_list[527] = "sys_rctl_get_limits";
    sysent_list[528] = "sys_rctl_add_rule";
    sysent_list[529] = "sys_rctl_remove_rule";
    sysent_list[530] = "sys_posix_fallocate";
    sysent_list[531] = "sys_posix_fadvise";
    sysent_list[532] = "sys_wait6";
    sysent_list[533] = "sys_cap_rights_limit";
    sysent_list[534] = "sys_cap_ioctls_limit";
    sysent_list[535] = "sys_cap_ioctls_get";
    sysent_list[536] = "sys_cap_fcntls_limit";
    sysent_list[537] = "sys_cap_fcntls_get";
    sysent_list[538] = "sys_bindat";
    sysent_list[539] = "sys_connectat";
    sysent_list[540] = "sys_chflagsat";
    sysent_list[541] = "sys_accept4";
    sysent_list[542] = "sys_pipe2";
    sysent_list[543] = "sys_aio_mlock";
    sysent_list[544] = "sys_procctl";
    sysent_list[545] = "sys_ppoll";
    sysent_list[546] = "sys_futimens";
    sysent_list[547] = "sys_utimensat";
    sysent_list[548] = "sys_numa_getaffinity";
    sysent_list[549] = "sys_numa_setaffinity";
    sysent_list[550] = "sys_fdatasync";
    //sysent_list[551] = "sys_MAXSYSCALL";
    //printf("AUE_NULL = %p\n",(void *)nosys);
    int counter = 0;
    errno = 0;

	//FILE *f;
	//f = fopen("out.txt", "w");
    	for(int i = 1; i<SYS_MAXSYSCALL; i++) {
		printf("Checking %d\n",i);
		if (is_blacklisted(i))
			continue;

		if(sysent_list[i] != NULL) {
			if (is_hooked(i, sysent_list[i], false)) {
				printf("Hooked at %d\n",i);
				return 1;
			}
		} else {
			//printf("%d\n", NO_SYSCALL);
			/*if (!firstMatched && i != NO_SYSCALL) {
				firstMatched = true;
				printf("NO_SYSCALL does not match actuall first empty syscall.");
				break;
			}*/
			//fprintf(f, "%d,", i);
			if (is_hooked(i, sysent_list[i], true)) {
				printf("Empty syscall %d seems hooked.... bummer.\n",i);
				return 1;
			}

			// check for ENOSYS
			/*int j = syscall(i);
			if (errno != ENOSYS) {
				// this should not be hooked...
				printf("Found not ENOSYS at %d\n", i);
				return 1;
			}*/
		}
	}
	return 0;
}
