#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

const char * binaries[] = {
    "./binaries/checkcall",
    "./binaries/loader",
    "./binaries/log_detector"
};

#define n_binaries (sizeof(binaries) / sizeof(const char *))


int main(int argc, char **argv) {
    chdir("./src/kland_sysent_detector");
    system("make");
    chdir("../..");
    system("cp -R ./src/kland_sysent_detector/* ./binaries");

    int status;
    for(int i = 0; i < n_binaries; i++) {
	status = system(binaries[i]);
	if ( WIFEXITED(status) && WEXITSTATUS(status) == 1) {
		printf("Fishy fishy...\n");
		return 1;
	} else if (WIFSIGNALED(status)) {
		//
	} 
    }
    printf("Nothing detected...\n");
    return 0;
}
