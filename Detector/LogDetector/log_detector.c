#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char * to_match[] = {
    "sudo ./escalate",
    // "sudo ./install",
    "kldload",
    "kldunload"
};

const char * logs_to_check[] = {
    "/var/log/setuid.today",
    "/var/log/setuid.yesterday",
    "/root/.history",
    "/var/log/auth.log",
    "/etc/crontab",
    "/boot/loader.conf"
};

const char * suid_logs[] = {
    "/var/log/setuid.today",
    "/var/log/setuid.yesterday"
};

const char * valid_suid_entries[] = {
    "/bin",
    "/sbin",
    "/usr/bin",
    "/usr/sbin",
    "/usr/local/bin",
    "/usr/sbin",
    "/usr/libexec",
    "/usr/local/libexec"
};

#define n_suid_entries (sizeof(valid_suid_entries) / sizeof (const char *))
#define n_suid_logs (sizeof(suid_logs) / sizeof (const char *))
#define n_logs (sizeof (logs_to_check) / sizeof (const char *))
#define n_array (sizeof (to_match) / sizeof (const char *))

int is_valid_suid_entry(char *line) {
    const char space = ' ';
    char *chptr = strrchr(line, space);
    if (chptr == NULL) return 1;

    chptr = chptr + 1;

    for(int i = 0; i < n_suid_entries; i++) {
        size_t cmplen = strlen(valid_suid_entries[i]);
        if (strncmp(chptr, valid_suid_entries[i], cmplen) == 0) // matches valid suid entry
            return 1;
    }
    return 0;
}

int is_suid_log(char *log) {
    for(int i = 0; i < n_suid_logs; i++) {
        if (strcmp(suid_logs[i],log) == 0)
            return 1;
    }
    return 0;
}

int line_matches(int llen, char *line, char *substr) {
    return (strstr(line, substr) != NULL);
}

int search(int llen, char *line) {
    for(int i = 0; i < n_array; i++)
    if (line_matches(llen,line,to_match[i]))
    return 1;

    return 0;
}

int main(int argc, char**argv) {
    FILE * fp;
    char * line;
    size_t len;
    ssize_t read;

    for(int i = 0; i < n_logs; i++) {
        line = NULL;
        len = 0;

        fp = fopen(logs_to_check[i], "r");

        if (fp == NULL) continue; // file doesn't exist

        while ((read = getline(&line, &len, fp)) != -1) {
            if (search(read, line)) {
                printf("Search match\n");
                printf("%s", line);
                return 1;
            }

            if (is_suid_log(logs_to_check[i])) {
                if (!is_valid_suid_entry(line)) {
                    printf("Invalid suid match : %s\n", logs_to_check[i]);
                    printf("%s",line);
                    return 1;
                }
            }
        }

        fclose(fp);
        if (line) free(line);
    }
    return 0;
}
