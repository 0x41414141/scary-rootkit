# Rootkit Assignment
**Team:** `41414141`

## Rootkit Installation
The rootkit is comprised of multiple LKM’s (Loadable Kernel Modules) which are loaded on installation.

All modules are precompiled binaries so they are ready to be loaded into the kernel.

The installation is done in x steps:
1. The `rootkit.tar` file is loaded onto the target server and extracted.
2. Each module is installed using `sudo kldload ./<module_name>.ko`
    1. `sudo kldload ./RootEsc.ko`
    2. `sudo kldload ./PortConcealer.ko`
    3. `sudo kldload ./ProcessConcealer.ko`
    4. `sudo kldload ./ModuleHider.ko`
3. The rootkit folder is then removed using `rm -rf <folder_path>`
4. The `rootkit.tar` file is removed

The LKMs are now loaded into the kernel and are hidden, allowing a non-root user to get a root shell without the use of a password.

## Privilege Escalation
The privilege escalation module creates a backdoor on the filesystem by creating a new `syscall` which is passed its own process id that it then sets it’s `uid` and `gid` to `0` respectively to elevate to root.

To get a root shell, a non-root user can call the created system call and then spawn a shell with the newly acquired privileges. He will then be able to perform any actions as root until the process ends or is killed off.

## Rootkit Concealment
The Rootkit will be concealed primarily by the `ModuleHider.ko` module which will hide all the other loaded modules:
* `RootEsc.ko`
* `PortConcealer.ko`
* `ProcessConcealer.ko`
including itself, by removing their entries in the `linker_files` list, and the `modules` list in the kernel's lists.

Furthermore, after the installation, we will essentially "delete" the binary files from the file system as they should be loaded in memory, leaving no footprint for `find` etc. as there will be no physical file left. Naturally, when we attempt the persistence, we will have to reverse this design.

## Process Concealer
A rudimentary Process Concealer (`ProcessConcealer.ko`) has been developed based on the basic example from the course textbook (redux version) which takes in a process id as an argument and hides it by removing it from the `allprocs` table and `pidhashbl` table. The `nprocs` variable is decremented to adjust the process count as well as removing it from the process group. 

## Port Concealer 
A rudimentary Port Concealer (`PortConcealer.ko`) has been developed based on the basic example from the course textbook which takes in a port number of a TCP connection as an argument and hides the connection by removing it from the `tcbinfo` list. 

## Bonus Marks to Be Attempted
We are aiming to attempt all the bonus marks based on the following priorities (according to time and difficulty):
* Remote Access
* Keylogging
* Persistence
While the aim is to achieve all three, we will do it in this order in case we run out of time.

## Future Components
We are aiming to implement the following for the final:
* The Bonus Marks components
* Tying the `PortConcealer.ko` module to the Remote Access bonus mark to hide the access
* Potentially using Port Hooking to help hide the hidden port
* Improve the `PortConcealer.ko` module by extending it from hiding both the server and the port to server and/or the port.

